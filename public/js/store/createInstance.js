const state = {
  flavorList: [],
};

const mutations = {
  addFlavorItem (state, item) {
    state.flavorList.push(item);
  },
};

const actions = {
  ADD_FLAVOR_ITEM ({ commit }, item) {
    commit('addFlavorItem', item)
  },
};

const store = new Vuex.Store({
  state,
  mutations,
  actions
})
