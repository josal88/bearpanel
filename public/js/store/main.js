const state = {
  networkList:    [],
  routerList:     [],
  instanceList:   [],
  volumeList:     [],
  imageList:      [],
  secGroupsList:  [],
  allGroupRules:  [],
  currentGroupRules: [],
};

const mutations = {
  addNetworkItem (state, item) {
    state.networkList.push(item);
  },
  resetNetworkList (state) {
    state.networkList = [];
  },
  addRouterItem (state, item) {
    state.routerList.push(item);
  },
  addInstanceItem (state, item) {
    state.instanceList.push(item);
  },
  addVolumeItem (state, item) {
    state.volumeList.push(item);
  },
  addImageItem (state, item) {
    state.imageList.push(item);
  },
  addSecGroupItem (state, item) {
    state.secGroupsList.push(item);
  },
  addAllGroupRules (state, item) {
    state.allGroupRules.push(item);
  },
  addCurrentGroupRules (state, item) {
    state.currentGroupRules.push(item);
  },
  resetSecGroup (state) {
    state.currentGroupRules = [];
  },
};

const actions = {
  ADD_NETWORK_ITEM ({ commit }, item) {
    commit('addNetworkItem', item)
  },
  RESET_NETWORK_LIST ({ commit }) {
    commit('resetNetworkList')
  },
  ADD_ROUTER_ITEM ({ commit }, item) {
    commit('addRouterItem', item)
  },
  ADD_INSTANCE_ITEM ({ commit }, item) {
    commit('addRouterItem', item)
  },
  ADD_VOLUME_ITEM ({ commit }, item) {
    commit('addVolumeItem', item)
  },
  ADD_IMAGE_ITEM ({ commit }, item) {
    commit('addImageItem', item)
  },
  ADD_ALL_SECGROUP_ITEM ({ commit }, item) {
    commit('addAllGroupRules', item)
  },
  ADD_CURRENT_SECGROUP_ITEM ({ commit }, item) {
    commit('addCurrentGroupRules', item)
  },
  RESET_SECGROUP ({ commit }) {
    commit('resetSecGroup');
  },
};

const store = new Vuex.Store({
  state,
  mutations,
  actions
})
