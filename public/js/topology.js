window.onload = function() {
  var canvas = document.getElementById("topologyObject"),
      context = canvas.getContext("2d"),
      width = canvas.width = 500,
      height = canvas.height = 500,

      centerX = width / 2,
      centerY = height / 2,
      radius = 20,
      angle = 0,
      speed = .01,
      x, y;

  render();

  function render() {
    context.clearRect(0, 0, width, height);
    x = centerX + Math.cos(angle) * radius * 2;
    y = centerY + Math.sin(angle) * radius * 2;

    context.beginPath();
    context.arc(x, y, 10, 0, Math.PI * 2, false);
    context.fillStyle = "red";
    context.fill();

    context.beginPath();
    context.arc(x + 20, y + 40, 10, 0, Math.PI * 2, false);
    context.fillStyle = "red";
    context.fill();

    context.beginPath();
    context.arc(x + 40, y + 10, 10, 0, Math.PI * 2, false);
    context.fillStyle = "red";
    context.fill();

    context.beginPath();
    context.arc(x + 40, y - 40, 10, 0, Math.PI * 2, false);
    context.fillStyle = "blue";
    context.fill();

    context.beginPath();
    context.moveTo(x, y);
    context.lineTo(centerX + Math.cos(angle) * radius, centerY + Math.cos(angle) * radius);
    context.stroke();

    context.beginPath();
    context.moveTo(x + 20, y + 40);
    context.lineTo(centerX + Math.cos(angle) * radius, centerY + Math.cos(angle) * radius);
    context.stroke();

    context.beginPath();
    context.moveTo(x + 40, y - 40);
    context.lineTo(centerX + Math.cos(angle) * radius, centerY + Math.cos(angle) * radius);
    context.stroke();

    context.beginPath();
    context.moveTo(x + 40, y + 10);
    context.lineTo(centerX + Math.cos(angle) * radius, centerY + Math.cos(angle) * radius);
    context.stroke();

    context.beginPath();
    context.arc(centerX + Math.cos(angle) * radius, centerY + Math.cos(angle) * radius, 10, 0, Math.PI * 2, false);
    context.fillStyle = "green";
    context.fill();
    angle += speed;
    requestAnimationFrame(render);
  }
}
