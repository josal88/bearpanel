new Vue({
  el: '#router',
  data: {
    subnets: {
      items: [],
      have: {
        loaded: false,
      },
    },
    networks: {
      items: [],
      have: {
        loaded: false,
      },
    },
    choosen: {
      interface: {
        subnet_id: [],
      },
      gateway: {
        subnet_id: [],
      }
    },
  },
  computed: {
  },
  methods: {
    addInterface: function(e) {
      var data = {
        _token: window.Laravel.csrfToken,
        subnet_id: this.choosen.interface.subnet_id,
      };
      $.post('/create/interface/' + e.target.name, data, function(res) {
        if(res === 200) {
          alert('This action was successful')
        } else {
          alert(res.NeutronError.message)
        }
      });
    },
    addGateway: function(e) {
      var data = {
        _token: window.Laravel.csrfToken,
        subnet_id: this.choosen.gateway.subnet_id,
      };
      $.post('/create/gateway/' + e.target.name, data, function(res) {
        if(res === 200) {
          alert('This action was successful')
        } else {
          alert(res.NeutronError.message)
        }
      });
    },
  },
  ready: function() {
    var self = this;
    $.get('/api/subnets', function(data) {
      data.subnets.map(function(network) {
        self.subnets.items.push(network)
      });
      self.subnets.have.loaded = true;
    });
    $.get('/api/networks', function(data) {
      data.networks.map(function(network) {
        self.networks.items.push(network)
      });
      self.networks.have.loaded = true;
    });
  }
});
