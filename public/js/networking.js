new Vue({
  el: '#networking',
  data: {
    req: {
      vlan: {
        network: {
          name: "Your network name",
          state: [],
          share: [],
          with_subnet: true,
          vlan_transparent: true
        },
        subnet: {
          name: [],
          netaddr: [],
          ipv: [],
          gateway: [],
        },
        details: {
          allocation_pool: [],
          dns: "8.8.8.8",
          host: [],
          enable_dhcp: true
        },
      },
      router: {
        name: [],
        state: [],
        network: []
      }
    },
    res: {
      created: [],
    },
    networks: {
      available: [],
      choosen: [],
      have: {
        loaded: false,
        selected: false
      }
    },
    networkDetails: {
      available: [],
      choosen: [],
      have: {
        loaded: false,
        selected: false
      }
    },
    routers: {
      available: [],
      choosen: [],
      have: {
        loaded: false,
        selected: false
      }
    },
    routersDetails: {
      available: [],
      choosen: [],
      have: {
        loaded: false,
        selected: false
      }
    },
    showNetworkPanel: false,
    showRouterPanel: false,
    showEditNetwork: false,
    currentNetworkEditReady: false,
    currentNetworkEdit: [],
    alertSuccess: false,
    alertError: false,
    messages: {
      successMsg: '',
      errorMsg: '',
    },
  },
  computed: {
    networkList: function() {
      return store.state.networkList;
    },
    routerList: function() {
      return store.state.routerList;
    },
  },
  methods: {
    /**
     *  AJAX Request further details on specific network
     *  and store that data.
     */
    moreNetworkInfo: function(id) {
      var self = this;
      $.get('/api/network/' + id, function(data) {
        self.networkDetails.available = data.network;
        self.networkDetails.have.loaded = true;
      });
    },
    /**
     *  Called if a new network is to be created.
     *  Gathers data and prepares POST request to API Gateway.
     */
    processNewNetwork: function() {
      var self = this;
      var data = {
        _token: window.Laravel.csrfToken,
        data: self.req.vlan
      };
      $.post('/create/network', data, function(data) {
        store.dispatch('ADD_NETWORK_ITEM', data[0].network);
      });
    },
    /**
     *  Called if a new router is to be created.
     *  Gathers data and prepares POST request to API gateway
     */
    processNewRouter: function() {
      var self = this;
      var data = {
        _token: window.Laravel.csrfToken,
        data: self.req.router
      };
      $.post('/create/router', data, function(data) {
        store.dispatch('ADD_ROUTER_ITEM', data.router);
      });
    },
    /**
     *  Flicker the switch to enable or disable displaying
     *  of panel.
     */
    displayNetworkPanel: function(e) {
      if(this.showEditNetwork)
        this.showEditNetwork = !this.showEditNetwork;

      this.showNetworkPanel = !this.showNetworkPanel;
      if(this.showNetworkPanel) {
        e.target.className = "fa fa-chevron-down pull-right";
      } else {
        e.target.className = "fa fa-chevron-up pull-right";
      }
    },
    displayRouterPanel: function(e) {
      this.showRouterPanel = !this.showRouterPanel;
      if(this.showRouterPanel) {
        e.target.className = "fa fa-chevron-down pull-right";
      } else {
        e.target.className = "fa fa-chevron-up pull-right";
      }
    },
    /**
     *  Edit network information
     */
    updateNetwork: function(id) {
      var self = this;
      var data = {
        name: this.currentNetworkEdit.name,
        router: this.currentNetworkEdit.router,
        admin_state_up: this.currentNetworkEdit.admin_state_up,
        port_security_enabled: this.currentNetworkEdit.port_security_enabled,
      }
      new Promise(function(resolve, reject) {
        $.post('/networking/update/' + id, { _token: window.Laravel.csrfToken, data: data }, function(res) {
          if(res == 200) 
          {
            resolve(res)
          }
          else
          {
            reject("Unable to process request: " + res);
          }
        });
      }).then(function(res) {
        // If we were successful in processing the request this is where we
        // end up. Form here we need to parse success message and then rewrite
        // our store for the new updates to be displayed!
        self.messages.successMsg = "Request successfully processed";
        self.alertSuccess = true;
        // We're resetting the store
        self.networks.have.loaded = false;
        store.dispatch('RESET_NETWORK_LIST');

      }).then(function() {
        // Refetch the new network list and commit changes
        $.get('/api/networks', function(data) {
          data.networks.map(function(network) {
            store.commit('addNetworkItem', network);
          });
          self.networks.have.loaded = true;
        });
      }).catch(function(e) {
        self.alertError = true;
        self.messages.errorMsg = e;
      });
    },
    editNetwork: function(id) {
      var self = this;
      if(self.currentNetworkEditReady)
        self.currentNetworkEditReady = !self.currentNetworkEditReady;
      // Fetch Network details
      new Promise(function(resolve, reject) {
        $.get('/api/network/' + id, function(res) {
          if(res.network) {
            resolve(res.network);
          } 
          else {
            reject("Unable to fetch network details.")
          } 
        });
      }).then(function(network) {
        // Parse network details
        self.currentNetworkEdit = network;
        // Set network editor to ready
        self.currentNetworkEditReady = true;
      }).catch(function(e) {
        self.alertError = true;
        self.messages.errorMsg = e;
      });
      // Disable new network button
      // Enable update network button
      this.showEditNetwork = true;
      // Flicker displayNetworkPanel, which now
      if(!this.showNetworkPanel)
        this.showNetworkPanel = !this.showNetworkPanel;
    },
    /**
     *  Error messages related
     */
    dismissErrorMsg: function() {
      this.alertError = !this.alertError;
      this.messages.errorMsg = '';
    },
    dismissSucessMsg: function() {
      this.alertSuccess = !this.alertSuccess;
      this.messages.successMsg = '';
    }
  },
  ready: function() {
    var self = this;
    /**
     *  Get and store network items for listing
     */
    $.get('/api/networks', function(data) {
      data.networks.map(function(network) {
        store.commit('addNetworkItem', network);
      });
      self.networks.have.loaded = true;
    });
    /**
     *  Get and store router items for listing
     */
    $.get('/api/routers', function(data) {
      data.routers.map(function(router) {
        store.commit('addRouterItem', router);
      });
      self.routers.have.loaded = true;
    });
  }
});
