new Vue({
  el: '#compute',
  data: {
    panels: {
      overview: true,
      instances: false,
      volumes: false,
      images: false,
      security: false,
      last: []
    },
    instance: {
      data: {
        labels: [
          "Unused",
          "Used",
        ],
        datasets: [
        {
          data: [300, 600],
          backgroundColor: [
            "#FF6384",
            "#36A2EB"
          ],
          hoverBackgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56"
          ]
        }]
      },
      options: {

      }
    },
    vcpus: {
      data: {
        labels: [
          "Unused",
          "Used",
        ],
        datasets: [
        {
          data: [300, 50],
          backgroundColor: [
            "#FF6384",
            "#36A2EB"
          ],
          hoverBackgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56"
          ]
        }]
      },
      options: {

      }
    },
    ram: {
      data: {
        labels: [
          "Unused",
          "Used",
        ],
        datasets: [
        {
          data: [50, 300],
          backgroundColor: [
            "#FF6384",
            "#36A2EB"
          ],
          hoverBackgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56"
          ]
        }]
      },
      options: {

      }
    },
    floatingIp: {
      data: {
        labels: [
          "Unused",
          "Used",
        ],
        datasets: [
        {
          data: [300, 100],
          backgroundColor: [
            "#FF6384",
            "#36A2EB"
          ],
          hoverBackgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56"
          ]
        }]
      },
      options: {

      }
    },
    securityGroups: {
      data: {
        labels: [
          "Unused",
          "Used",
        ],
        datasets: [
        {
          data: [300, 70],
          backgroundColor: [
            "#FF6384",
            "#36A2EB"
          ],
          hoverBackgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56"
          ]
        }]
      },
      options: {

      }
    },
    volumes: {
      data: {
        labels: [
          "Unused",
          "Used",
        ],
        datasets: [
        {
          data: [300, 50],
          backgroundColor: [
            "#FF6384",
            "#36A2EB"
          ],
          hoverBackgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56"
          ]
        }]
      },
    },
    api: {

    },
    edit: {
      instance: {
        old: [],
        new: [],
      },
    },
    loadedInstance:  false,
    loadedInstances: false,
    loadedImages:    false,
    loadedVolumes:   false,
    loadedSecGroups: false,
    loadedSecGroupsRules: false,
    currentSecGroupId: [],
    tables: {
      instances:    [],
      instance:     [],
      images:       [],
      volumes:      [],
      volume:       [],
      currentRules: [],
      allRules:     [],
    },
    addNewRule: {
      protocol: [],
    },
    addRuleUI: {
      makePortRange: false,
      makePort: true,
    }
  },
  computed: {
    listInstances: function() {
      return store.state.instanceList;
    },
    listVolumes: function() {
      return store.state.volumeList;
    },
    listImages: function() {
      return store.state.imageList;
    },
    listSecGroups: function() {
      return store.state.secGroupsList;
    },
    listAllAvailableRules: function() {
      return store.state.allGroupRules;
    },
    listCurrentRules: function() {
      return store.state.currentGroupRules;
    },
  },
  methods: {
    /**
     *  Compute API
     */
    deleteInstance: function(e) {
      var self = this;
      $.get('/delete/server/' + e.target.name, function(data) {
        if(data.deleteSuccess) {
          self.loadedInstances = false;
          self.tables.instances.length = 0;
          $.get('/api/servers', function(data) {
            self.tables.instances = data.servers;
            self.loadedInstances = true;
          });
        }
      });
    },
    getInstanceInfo: function(e) {
      var self = this;
      $.get('/api/server/' + e.target.id, function(data) {
        self.tables.instance = data;
        $.get('/api/flavor/' + self.tables.instance.server.flavor.id, function(data) {
          self.tables.flavor = data.flavor.name;
          self.loadedInstance = true;
        });
      });
    },
    viewOverview: function(e) {
      this.panels.overview = !this.panels.overview;
      if($('#overview').hasClass("active")) {
        $('#overview').removeClass("active")
      } else {
        $('#overview').addClass("active")
      }
    },
    getVolumeInfo: function(e) {
      var self = this;
      $.get('/api/volumes', function(data) {
        for (var i = 0; i < data.volumes.length; i++) {
          if(data.volumes[i].id == e.target.name) {
            self.tables.volume = data.volumes[i];
          }
        }
        self.loadedVolumes = true;
      });
    },
    viewInstances: function(e) {
      var self = this;
      this.panels.instances = !this.panels.instances;
      if($('#instances').hasClass("active")) {
        $('#instances').removeClass("active")
      } else {
        $('#instances').addClass("active")
      }
      this.panels.last = $(e.target).parent('#button');
    },
    viewVolumes: function(e) {
      var self = this;
      this.panels.volumes = !this.panels.volumes;
      if($('#volumes').hasClass("active")) {
        $('#volumes').removeClass("active")
      } else {
        $('#volumes').addClass("active")
      }
      this.panels.last = $(e.target).parent('#button');
    },
    viewImages: function(e) {
      var self = this;

      this.panels.images = !this.panels.images;
      if($('#images').hasClass("active")) {
        $('#images').removeClass("active")
      } else {
        $('#images').addClass("active")
      }
      this.panels.last = $(e.target).parent('#button');
    },
    /**
     *  Security related
     */
    viewSecurity: function(e) {
      this.panels.security = !this.panels.security;
      if($('#security').hasClass("active")) {
        $('#security').removeClass("active")
      } else {
        $('#security').addClass("active")
      }
      this.panels.last = $(e.target).parent('#button');
    },
    //  Get security rules from specified group
    getSecurityRules: function(id) {
      var self = this;
      // Check if id is loaded or not to prevent list getting
      // duplicated items i.e. load items to store or not
      if(!this.currentSecGroupId || this.currentSecGroupId != id) 
      {
        this.currentSecGroupId = id;
        store.dispatch('RESET_SECGROUP');
        new Promise(function(resolve, reject) 
        {
          $.get('/api/security/rules/' + id, function(data) 
          {
            if(data.security_group.security_group_rules) 
            {
              resolve(data.security_group.security_group_rules)
            } 
            else 
            {
              reject("Unable to fetch group rules")
            }
          });
        }).then(function(rules) 
        {
          rules.map(function(rule) 
          {
            store.commit('addCurrentGroupRules', rule);
          });
          self.loadedSecGroupsRules = true;
        }).catch(function(e) 
        {
          alert("Woops, error: " + e);
        });
      }
    },
    /**
     *  Add security group UI related
     */
    makePortRange: function() {
      if(this.addRuleUI.makePort) 
      {
        this.addRuleUI.makePort = !this.addRuleUI.makePort;
      }
      this.addRuleUI.makePortRange = !this.addRuleUI.makePortRange;
    },
    makePort: function() {
      if(this.addRuleUI.makePortRange) 
      {
        this.addRuleUI.makePortRange = !this.addRuleUI.makePortRange;
      }
      this.addRuleUI.makePort = !this.addRuleUI.makePort;
    },
    // If Port was choosen we set port max to same as port min
    addPortMax: function(e) {
      this.addNewRule.port_max = this.addNewRule.port_min
    },
    addSecurityRule: function() {
      console.log(this.addNewRule);
      var self = this;
      var url = '/api/security/rules/' + this.currentSecGroupId;
      var data = {
        _token: window.Laravel.csrfToken,
        data: self.addNewRule,
      };
      new Promise(function(resolve, reject) 
      {
        $.post(url, data, function(data) 
        {
          alert(data)
        });
      });
    },
    /**
     *  Manage instance
     */
    startInstance: function(id) {
      alert("clicked")
      new Promise(function(resolve, reject) {
        $.get('/api/server/start/' + id, function(res) {
          if(res == 202) {
            resolve(res);
          } else {
            reject(res)
          }
        });
      }).then(function() {
        alert("Instance was successfully started!");
      }).catch(function(res) {
        if(res == 401) 
        {
          alert("You're not authorized to perform this action!")
        } 
        else if(res == 403) 
        {
          alert("This action is forbidden.")
        }
        else if(res == 404)
        {
          alert("Woops, this is ackward! We couldn't find the resource you're looking for!")
        }
        else if(res == 409)
        {
          alert("There was a conflict encountered while trying to perform the requested action!");
        }
        else {
          alert("Woops, this is ackward! Unable to perform action!")
        }
      });
    },
    restartInstance: function(id) {
      alert("clicked")
      new Promise(function(resolve, reject) {
        $.get('/api/server/restart/' + id, function(res) {
          if(res == 202) 
          {
            resolve(res);
          }
          else
          {
            reject(res)
          }
        });
      }).then(function() 
      {
        alert("Instance was successfully restarted!");
      }).catch(function(res) 
      {
        if(res == 401) 
        {
          alert("You're not authorized to perform this action!")
        } 
        else if(res == 403) 
        {
          alert("This action is forbidden.")
        }
        else if(res == 404)
        {
          alert("Woops, this is ackward! We couldn't find the resource you're looking for!")
        }
        else if(res == 409)
        {
          alert("There was a conflict encountered while trying to perform the requested action!");
        }
        else {
          alert("Woops, this is ackward! Unable to perform action!")
        }
      });
    },
    shutdownInstance: function(id) {
      alert("clicked")
      new Promise(function(resolve, reject) {
        $.get('/api/server/pause/' + id, function(res) {
          if(res == 202) {
            resolve(res);
          } else {
            reject(res)
          }
        });
      }).then(function() {
        alert("Instance was successfully paused!");
      }).catch(function(res) {
        if(res == 401) 
        {
          alert("You're not authorized to perform this action!")
        } 
        else if(res == 403) 
        {
          alert("This action is forbidden.")
        }
        else if(res == 404)
        {
          alert("Woops, this is ackward! We couldn't find the resource you're looking for!")
        }
        else if(res == 409)
        {
          alert("There was a conflict encountered while trying to perform the requested action!");
        }
        else {
          alert("Woops, this is ackward! Unable to perform action!")
        }
      });
    },
    shutdownInstance: function(id) {
      alert("clicked")
      new Promise(function(resolve, reject) {
        $.get('/api/server/shutdown/' + id, function(res) {
          if(res == 202) 
          {
            resolve(res);
          } else 
          {
            reject(res)
          }
        });
      }).then(function() {
        alert("Instance was successfully shutdown!");
      }).catch(function(res) {
        if(res == 401) 
        {
          alert("You're not authorized to perform this action!")
        } 
        else if(res == 403) 
        {
          alert("This action is forbidden.")
        }
        else if(res == 404)
        {
          alert("Woops, this is ackward! We couldn't find the resource you're looking for!")
        }
        else if(res == 409)
        {
          alert("There was a conflict encountered while trying to perform the requested action!");
        }
        else {
          alert("Woops, this is ackward! Unable to perform action!")
        }
      });
    },
    /**
     *  Edit instances
     */
    editInstanceName: function(e) {
      this.edit.instance.old = e.target.innerHTML.trim();
      $(e.target).replaceWith(function() {
        return $("<input id='updateName' class='form-control' type='text' value='"+e.target.innerHTML.trim()+"'/>");
      });
    },
    updateInstanceName: function(e) {
      var self = this;
      var id;
      var path = e.path || e.target.parentNode;

      if(typeof path[1] !== 'undefined') {
        id = path[1].title;
      } else {
        id = path.title
      }

      if(typeof path[0] !== 'undefined') {
        this.edit.instance.new = path[0].value
      } else {
        this.edit.instance.new = e.target.value;
      }

      $.post('/update/server/' + id, {
        _token: window.Laravel.csrfToken,
        data: self.edit.instance.new
      }, function(data) {
        $(e.target).replaceWith(function() {
          return $("<span/>", {html: self.edit.instance.new});
        });
      });
    },
  },
  ready: function() {
    var self = this;
    /**
     *  Initilise
     */
     $.get('/api/servers', function(data) {
       data.servers.map(function(server) {
         store.commit('addInstanceItem', server)
       });
       self.loadedInstances = true;
     });

     $.get('/api/volumes', function(data) {
       data.volumes.map(function(volume) {
         store.commit('addVolumeItem', volume)
       });
       self.loadedVolumes = true;
     });

     $.get('/api/images', function(data) {
       data.images.map(function(image) {
         store.commit('addImageItem', image)
       });
       self.loadedImages = true;
     });

     $.get('/api/security/groups', function(data) {
       data.security_groups.map(function(group) {
         store.commit('addSecGroupItem', group);
       });
       self.loadedSecGroups = true;
     });

     $.get('/api/security/rules', function(data) {
       data.security_group_rules.map(function(rule) {
         store.commit('addAllGroupRules', rule);
       });
       self.loadedSecRules = true;
     });
    /**
     *  Modals
     */
     $('#myModal').on('shown.bs.modal', function () {
       $('#myInput').focus()
     });
    /**
     *  Creating doughnut charts!
     */
    var ctx = document.getElementById("instanceChart");
    var ctx = document.getElementById("instanceChart").getContext("2d");
    var ctx = $("#instanceChart");
    var instanceChart = new Chart(ctx, {
      type: 'doughnut',
      data: this.instance.data,
      options: this.instance.options
    });
    var ctxVcpu = document.getElementById("vcpusChart");
    var ctxVcpu = document.getElementById("vcpusChart").getContext("2d");
    var ctxVcpu = $("#vcpusChart");
    var instanceChart = new Chart(ctxVcpu, {
      type: 'doughnut',
      data: this.vcpus.data,
      options: this.vcpus.options
    });
    var ctxRam = document.getElementById("ramChart");
    var ctxRam = document.getElementById("ramChart").getContext("2d");
    var ctxRam = $("#ramChart");
    var instanceChart = new Chart(ctxRam, {
      type: 'doughnut',
      data: this.ram.data,
      options: this.ram.options
    });
    var ctxFloatingIps = document.getElementById("floatingIpChart");
    var ctxFloatingIps = document.getElementById("floatingIpChart").getContext("2d");
    var ctxFloatingIps = $("#floatingIpChart");
    var instanceChart = new Chart(ctxFloatingIps, {
      type: 'doughnut',
      data: this.floatingIp.data,
      options: this.floatingIp.options
    });
    var ctxSecurityGroups = document.getElementById("securityGroupsChart");
    var ctxSecurityGroups = document.getElementById("securityGroupsChart").getContext("2d");
    var ctxSecurityGroups = $("#securityGroupsChart");
    var instanceChart = new Chart(ctxSecurityGroups, {
      type: 'doughnut',
      data: this.securityGroups.data,
      options: this.securityGroups.options
    });
    var ctxVolumes = document.getElementById("volumesChart");
    var ctxVolumes = document.getElementById("volumesChart").getContext("2d");
    var ctxVolumes = $("#volumesChart");
    var instanceChart = new Chart(ctxVolumes, {
      type: 'doughnut',
      data: this.volumes.data,
      options: this.volumes.options
    });
  }
});


/**
 *  Filters to be applied
 ****************************************************************************/

Vue.filter('momento', function (value) {
  return moment(value).startOf('day').fromNow();
});
