new Vue({
  el: '#createInstance',
  data: {
    system: {
      storage: 50,
      memory: 32,
      cpu: 1,
    },
    created: {
      server: {
          name : [],
          imageRef : [],
          flavorRef : [],
          availability_zone: "",
          diskConfig: "AUTO",
          metadata : {
              serverName: ""
          },
          personality: [
            {
              "path": "",
              "contents": ""
            }
          ],
          security_groups: [
              {
                  "name": "default"
              }
          ],
          key_name: [],
      },
      scheduler_hints: {
          same_host: []
      }
    },
    keypair: [],
    // END SERVER BODY TO REQUEST
    details: {
      nodes: 0,
    },
    availabilityzone: {
      available: [],
      choosen: [],
      have: {
        loaded: false,
        selected: false
      }
    },
    flavors: {
      available: [],
      choosen: [],
      exist: false,
      create: false,
      have: {
        loaded: false,
        selected: false
      }
    },
    sources: {
      available: [],
      choosen: [],
      have: {
        loaded: false,
        selected: false
      }
    },
    networks: {
      available: [],
      choosen: [],
      have: {
        loaded: false,
        selected: false
      }
    },
    secruitygroups: {
      available: [],
      choosen: [],
      have: {
        loaded: false,
        selected: false
      }
    },
    keypairs: {
      available: [],
      choosen: [],
      have: {
        loaded: false,
        selected: false
      }
    },
    hasBeenEncoded: false,
    onCreated: [],
    onCreatedLoaded: false,
    createKeypair: true,
    newServerInfo: [],
  },
  methods: {
    incNode: function() {
      this.details.nodes += 1;
    },
    decNode: function() {
      if(this.details.nodes <= 0) {
        this.details.nodes = 0;
      } else {
        this.details.nodes -= 1;
      }
    },
    fetchResource: function() {
      console.log(this.system);
      var self = this;
      $.post('/suggest/flavor', { 
        _token: window.Laravel.csrfToken, 
        data: self.system,
      }, function(data) {
        if(!data.length) {
          self.flavors.exist = false;
          self.flavors.create = true;
        } else {
          self.flavors.exist = true;
          self.flavors.available = data
        }
      });
    },
    createResource: function() {
      var self = this;
      $.post('/create/flavor', { 
        _token: window.Laravel.csrfToken, 
        data: self.system,
      }, function(data) {
        self.created.server.flavorRef = data.flavor.id;
        $.get('/api/flavor/' + self.created.server.flavorRef, function(data) {
          self.flavors.choosen = data;
          self.flavors.have.selected = true;
        });
      });
    },
    chooseFlavor: function(e) {
      this.created.server.flavorRef = e.target.name;
      $.get('/api/flavor/' + self.created.server.flavorRef, function(data) {
        self.flavors.choosen = data;
        this.flavors.have.selected = true;
      });
    },
    loadSource: function(data) {
      var self = this;
      $.get('/api/images', function(data) {
        self.sources.choosen = data.images;
        self.sources.have.selected = true;
      });
    },
    base64EncodeUserData: function() {
      this.created.server.user_data = atob(this.created.server.user_data)
      this.hasBeenEncoded = true;
    },
    uploadFile: function(e) {
      this.createKeypair = !this.createKeypair;
    },
    selectKeypair: function(e) {
      /* Clean up if there is already a key pair choosen */
      if(this.created.server.key_name.length) {
        this.created.server.key_name = [];
      }
      this.created.server.key_name = e.target.name;
    },
    submitServer: function() {
      var self = this;
      $.post('/create/server', { 
        _token: window.Laravel.csrfToken, 
        data: JSON.stringify(self.created), 
        keypair: this.keypair
      }, function(res) {
        if(res.badRequest) {
          alert(res.badRequest.message);
        } else {
          self.onCreated = res;
          self.onCreatedLoaded = true;
          $.get('/api/server/' + res.server.id, function(data) {
            self.newServerInfo = data;
          });
        }
      });
    },
  },
  ready: function() {
    $('#myModal').on('shown.bs.modal', function () {
      $('#myInput').focus()
    });
    var self = this;
    $.get('/api/availabilityzone', function(data) {
      data.availabilityZoneInfo.map(function(data) {
        if(data.zoneState.available) {
          self.availabilityzone.available.push(data);
        }
      });
      self.availabilityzone.have.loaded = true;
    });
    $.get('/api/images', function(data) {
      self.sources.available = data.images;
      self.sources.have.loaded = true;
    });
    $.get('/api/networks', function(data) {
      self.networks.available = data.networks;
      self.networks.have.loaded = true;
    });
    $.get('/api/secruitygroups', function(data) {
      self.secruitygroups.available = data.security_groups;
      self.secruitygroups.have.loaded = true;
    });
    $.get('/api/keypairs', function(data) {
      self.keypairs.available = data.keypairs;
      self.keypairs.have.loaded = true;
    });
  }
});

Vue.filter('crdTransform', function (value) {
  return value.replace(/^[C|R|D]/, '');
});

Vue.filter('CPU', function (value) {
  if(value == 1) {
    return value + " core";
  }
  return value + " cores";
});

Vue.filter('RAM', function (value) {
  if(value >= 1024) {
    return value / 1024 + "Gb";
  }
  return value + "Mb"
});

Vue.filter('DISK', function (value) {
  return value + "Gb";
});