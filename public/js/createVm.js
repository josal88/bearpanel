new Vue({
  el: '#createVm',
  data: {
    choosenOs: false,
    system: {
      storage: 1,
      memory:  32,
      cpu:     1,
      network: 10,
    },
    operatingSystem: true,
    instaApp: false,
    amountOfNodes: 1
  },
  methods: {
    chooseOs: function(e) {
      if(this.choosenOs) {
        var choosen = document.getElementById(e.target.id);
        choosen.style.border = '2px solid dodgerblue';
        this.choosenOs = !this.choosenOs; // False
      } else {
        this.choosenOs = !this.choosenOs; // True
      }
    },
    changeStorage: function(e) {
      console.log("Storage");
    },
    changeMemory: function(e) {
      console.log("Memory");
    },
    changeCpu: function(e) {
      console.log("Cpu");
    },
    changeNetwork: function(e) {
      console.log("Network");
    },
    displayInstaApp: function(e) {
      this.operatingSystem = !this.operatingSystem;
      this.instaApp = !this.instaApp;
      if(this.instaApp) {
        e.target.className = "fa fa-chevron-down pull-right";
      } else {
        e.target.className = "fa fa-chevron-up pull-right"
      }
    },
    removeNode: function() {
      if(this.amountOfNodes <= 0) {
        return false;
      }
      this.amountOfNodes -= 1;
    },
    addNode: function() {
      this.amountOfNodes += 1;
    }
  },
  ready: function() {
    $('input[type="range"]').rangeslider();
  }
});
