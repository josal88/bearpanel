new Vue({
  el: '#projects',
  data: {
    projects: []
  },
  methods: {

  },
  ready: function() {
    var self = this;
    /**
     *  Tenants are project and project are tenants, sometimes. This time...
     *  Name convensions are "loved" and "obeyed" by Openstack, not!
     */
    $.get('/tenants', function(data) {
        self.projects = data.tenants;
    });
  }
});
