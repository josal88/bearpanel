new Vue({
  el: '#navigation',
  data: {
    auth: false
  },
  methods: {
    loggedin: function() {

    }
  },
  ready: function() {
    var self = this;
    $.get('/authenticated', function(data) {
      if(data == 200) {
        self.auth = true;
      }
    });
  }
});
