# Bearpanel

Bearpanel is an alternative to Horizon when it comes to working with Openstack and the corresponding API. It's made using Laravel and
PHP together with VueJS for the frontend.
It's created with the intention to be available for partners to use a clouding solution with the standard functionality of Horizon 
rewrapped in a more user friendly manner.
## Official Documentation

None.

## Security Vulnerabilities

If you discover a security vulnerability within Bearpanel, please send an e-mail to Johan Saldes at johan.saldes@gmail.com. All security vulnerabilities will be promptly addressed.

## License

All rights reserved