<?php
namespace App\Jsal\Keystone;

class Keystone {

  /**
   *  @var _ch - Curl handler
   */
  private $_ch;

  private $_auth;

  private $_token;

  private $_tenant_id = null;

  /**
   *  Sets up our keystone verification HTTP request
   *  @param $auth
   *      json data containing username and password can be FALSE
   *  @param $endpoint
   *      API endpoint we want to access
   */
  private function _setop($curlopt)
  {
    try {
      curl_setopt($this->_ch, CURLOPT_URL, env('API_ACCESS') . $curlopt['endpoint']);
      curl_setopt($this->_ch, CURLOPT_CUSTOMREQUEST, $curlopt['method']);
      if($curlopt['auth'])
        curl_setopt($this->_ch, CURLOPT_POSTFIELDS, $curlopt['auth']);
      curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->_ch, CURLOPT_HEADER, $curlopt['returnHeader']);
      curl_setopt($this->_ch, CURLOPT_HTTPHEADER, $curlopt['headers']);
    } catch(Exception $e) {
      abort(403, 'Unauthorized.');
    }
  }

  private function getRealToken() {
    try {
      $this->_ch = curl_init();
      $this->_auth = json_encode($this->credentials());
      $this->_setop([
        'auth' => $this->_auth,
        'method' => 'POST',
        'returnHeader' => false,
        'headers' => ['Content-type: application/json'],
        'endpoint' => ":5000/v2.0/tokens"
      ]);
      $exec = json_decode(curl_exec($this->_ch));
      if(isset($exec->error->code)) {
        return false;
      }
      return $exec->access->token->id;
    } catch(Exception $e) {
      abort(403, 'Unauthorized.');
    }
  }

  private function _checkTenantId()
  {
    try {
      $this->_ch = curl_init();
      $headers = array(
        'X-Auth-Token:' . $this->_token
      );
      $this->_setop([
        'auth' => false,
        'method' => 'GET',
        'returnHeader' => false,
        'headers' => $headers,
        'endpoint' => ":5000/v2.0/tenants"
      ]);
      return last(json_decode(curl_exec($this->_ch))->tenants)->id;
    } catch(Exception $e) {
      abort(403, 'Unauthorized.');
    }
  }

  private function credentials() {
    return [ "auth" => [ "passwordCredentials" => [ "username" => "admin", "password" => env('ADMIN_PASS') ], "tenantId" => $this->_tenant_id ] ];
  }

  public function admin()
  {
    try {
      $this->_ch = curl_init();
      $this->_auth = json_encode($this->credentials());
      $headers = array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($this->_auth)
      );

      $this->_setop([
        'auth' => $this->_auth,
        'method' => 'POST',
        'returnHeader' => false,
        'headers' => $headers,
        'endpoint' => ":5000/v2.0/tokens"
      ]);

      $response = json_decode(curl_exec($this->_ch));
      $this->_token = $response->access->token->id;
      curl_close($this->_ch);
      $this->_tenant_id = $this->_checkTenantId();
      return $this->getRealToken();
    } catch(Exception $e) {
      abort(403, 'Unauthorized.');
    }
  }

}
?>
