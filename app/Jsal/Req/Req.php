<?php
namespace App\Jsal\Req;

class Req {

  /**
   *  @var _ch - Curl handler
   */
  private $_ch;

  /**
   *  $options = [
   *     'data' => json_encode($data),
   *     'method' => 'POST',
   *     'returnHeader' => false,
   *     'headers' => ['X-Auth-Token: ' . session('token'), 'Content-type: application/json'],
   *     'endpoint' => ":9696/v2.0/networks"
   *  ];
   */
  public function make($options)
  {
    try
    {
      $this->_ch = curl_init();

      curl_setopt($this->_ch, CURLOPT_URL, env('API_ACCESS') . $options['endpoint']);
      curl_setopt($this->_ch, CURLOPT_CUSTOMREQUEST, $options['method']);
      curl_setopt($this->_ch, CURLOPT_POSTFIELDS, $options['data']);
      curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->_ch, CURLOPT_HEADER, $options['returnHeader']);
      curl_setopt($this->_ch, CURLOPT_HTTPHEADER, $options['headers']);

      return curl_exec($this->_ch);
    }
    catch(Exception $e) {
      abort(403, 'Unauthorized.');
    }
  }

}
?>
