<?php
namespace App\Jsal\Logger;

class Logger {

  private $_filename = 'logger.log';
  private $_fp;


  public function __construct() {
    $this->_fp = fopen(base_path() . "/logger/" . $this->_filename, 'a');
  }

  public function write($string) {
    fwrite($this->_fp, "[ ".date("Y-m-d H:m:s")." ] " . $string ."\n");
  }

}
?>
