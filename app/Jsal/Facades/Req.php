<?php
namespace App\Jsal\Facades;

use Illuminate\Support\Facades\Facade;

class Req extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
      return 'keystone';
    }
}
?>
