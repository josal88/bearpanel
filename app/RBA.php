<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class RBA extends Eloquent
{
  protected $collection = 'compute';
  public $timestamps = false;
  protected $connection = 'mongodb';

  private $_auth;

  public function __construct() {
    if(!empty(session('token'))) {
      $this->_auth = session('token');
      // This is to be used later in order to verfiy user before
      // accessing model.
    }
  }

  /**
   *  Decides if the user is authorized or not.
   *  @param $userId user id as fetched from OS_KEYSTONE
   *  @param $action linked to action in mongo collection
   *  @return bool
   */
  public static function authWrite($userId, $action) {
    if(isset(RBA::where('action', $action)->where('users', $userId)->get()[0])) {
      return RBA::where('action', $action)->where('users', $userId)->get()[0]['write'];
    }
    return false;
  }

  public static function authRead($userId, $action) {
    if(isset(RBA::where('action', $action)->where('users', $userId)->get()[0])) {
      return RBA::where('action', $action)->where('users', $userId)->get()[0]['read'];
    }
    return false;
  }
}
