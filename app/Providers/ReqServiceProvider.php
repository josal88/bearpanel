<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ReqServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('req', 'App\Jsal\Req\Req');
    }
}
