<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Keystone;

class computeController extends Controller
{
    private $_ch;
    private $_auth;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('token');
    }

    private function _setopWithData($curlopt)
    {
      try {
        curl_setopt($this->_ch, CURLOPT_URL, env('API_ACCESS') . $curlopt['endpoint']);
        curl_setopt($this->_ch, CURLOPT_CUSTOMREQUEST, $curlopt['method']);
        curl_setopt($this->_ch, CURLOPT_POSTFIELDS, $curlopt['data']);
        curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->_ch, CURLOPT_HEADER, $curlopt['returnHeader']);
        curl_setopt($this->_ch, CURLOPT_HTTPHEADER, $curlopt['headers']);
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    private function _setop($auth, $method, $endpoint, $headers, $head)
    {
      try {
        curl_setopt($this->_ch, CURLOPT_URL, env('API_ACCESS') . $endpoint);
        curl_setopt($this->_ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->_ch, CURLOPT_HEADER, $head);
        curl_setopt($this->_ch, CURLOPT_HTTPHEADER, $headers);
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function index()
    {
      return view('pages.compute');
    }

    public function listInstances()
    {
      try {
        $this->_ch = curl_init();
        $headers = [
          'X-Auth-Token: ' . session('token'),
          'Accept: application/json'
        ];
        $this->_setop(session('token'), 'GET', ':8774/v2.1/servers', $headers, false);
        return response()->json(json_decode(curl_exec($this->_ch)));
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function getInstance($id)
    {
      try {
        $this->_ch = curl_init();
        $headers = [
          'X-Auth-Token: ' . session('token'),
          'Accept: application/json'
        ];
        $this->_setop(session('token'), 'GET', ':8774/v2.1/servers/' . $id, $headers, false);
        return response()->json(json_decode(curl_exec($this->_ch)));
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }
    public function listImages()
    {
      try {
        $this->_ch = curl_init();
        $headers = [
          'X-Auth-Token: ' . session('token'),
          'Accept: application/json'
        ];
        $this->_setop(session('token'), 'GET', ':9292/v2/images', $headers, false);
        return response()->json(json_decode(curl_exec($this->_ch)));
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }
    public function getImage($id)
    {
      try {
        $this->_ch = curl_init();
        $headers = [
          'X-Auth-Token: ' . session('token'),
          'Accept: application/json'
        ];
        $this->_setop(session('token'), 'GET', ':8774/v2/images/' . $id, $headers, false);
        return response()->json(json_decode(curl_exec($this->_ch)));
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function getFlavors()
    {
      try {
        $this->_ch = curl_init();
        $headers = [
          'X-Auth-Token: ' . session('token'),
          'Accept: application/json'
        ];
        $this->_setop(session('token'), 'GET', ':8774/v2/flavors', $headers, false);
        return response()->json(json_decode(curl_exec($this->_ch)));
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function getFlavorsWithDetails()
    {
      try {
        $this->_ch = curl_init();
        $headers = [
          'X-Auth-Token: ' . session('token'),
          'Accept: application/json'
        ];
        $this->_setop(session('token'), 'GET', ':8774/v2/flavors/detail', $headers, false);
        return response()->json(json_decode(curl_exec($this->_ch)));
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function getFlavor($id)
    {
      try {
        $this->_ch = curl_init();
        $headers = [
          'X-Auth-Token: ' . session('token'),
          'Accept: application/json'
        ];
        $this->_setop(session('token'), 'GET', ':8774/v2/flavors/' . $id, $headers, false);
        return response()->json(json_decode(curl_exec($this->_ch)));
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function createFlavor(Request $request)
    {
      try {
        $this->_ch = curl_init();

        $name = "C" . $request->data['cpu'] . "R" . $request->data['memory'] . "D" . $request->data['storage'] * 1024;

        $data = [
          "flavor" => [
            "name" => $name,
            "ram" => $request->data['memory'],
            "vcpus" => $request->data['cpu'],
            "disk" => $request->data['storage'],
          ],
        ];
        $options = [
          'data' => json_encode($data),
          'method' => 'POST',
          'returnHeader' => false,
          'headers' => ['X-Auth-Token: ' . Keystone::admin(), 'Content-type: application/json'],
          'endpoint' => ":8774/v2/flavors"
        ];
        $this->_setopWithData($options);
        $exec = curl_exec($this->_ch);
        \Logger::write(json_encode($exec) ."\n");
        curl_close($this->_ch);
        return response()->json(json_decode($exec));
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function getAvailabilityZone()
    {
      try {
        $this->_ch = curl_init();
        $headers = [
          'X-Auth-Token: ' . session('token'),
          'Accept: application/json'
        ];
        $this->_setop(session('token'), 'GET', ':8774/v2/os-availability-zone', $headers, false);
        return response()->json(json_decode(curl_exec($this->_ch)));
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function getSecurityGroups()
    {
      try {
        $this->_ch = curl_init();
        $headers = [
          'X-Auth-Token: ' . session('token'),
          'Accept: application/json'
        ];
        $this->_setop(session('token'), 'GET', ':9696/v2.0/security-groups', $headers, false);
        return response()->json(json_decode(curl_exec($this->_ch)));
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function getKeypairs()
    {
      try {
        $this->_ch = curl_init();
        $headers = [
          'X-Auth-Token: ' . session('token'),
          'Accept: application/json'
        ];
        $this->_setop(session('token'), 'GET', ':8774/v2/os-keypairs', $headers, false);
        return response()->json(json_decode(curl_exec($this->_ch)));
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }
    
    public function createServer(Request $request)
    {
      try {
        $this->_ch = curl_init();
        $data = json_decode($request->data);
        \Logger::write("(".__METHOD__.") Creating: ". json_encode($data));
        $options = [
          'data' => json_encode($data),
          'method' => 'POST',
          'returnHeader' => false,
          'headers' => ['X-Auth-Token: ' . session('token'), 'Content-type: application/json'],
          'endpoint' => ":8774/v2/servers"
        ];
        $this->_setopWithData($options);
        $exec = curl_exec($this->_ch);
        \Logger::write("(".__METHOD__.") Responded: ". json_encode($exec));
        curl_close($this->_ch);
        return response()->json(json_decode($exec));
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function deleteServer($id)
    {
      try {
        $this->_ch = curl_init();
        $headers = [
          'X-Auth-Token: ' . session('token'),
          'Accept: application/json'
        ];
        $this->_setop(session('token'), 'DELETE', ':8774/v2/servers/' . $id, $headers, false);
        curl_exec($this->_ch);
        $respCode = curl_getinfo($this->_ch, CURLINFO_HTTP_CODE);
        curl_close($this->_ch);
        if($respCode == 204) {
          return response()->json(['deleteSuccess' => true]);
        } else {
          return response()->json(['deleteSuccess' => false]);
        }
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function updateServerName(Request $request, $id)
    {
      try {
        \Logger::write("(".__METHOD__.") Input (".$id."): ". json_encode($request));
        $this->_ch = curl_init();
        $data = json_encode([
          "server" => [
              "name" => $request->data
          ]
        ]);
        $options = [
          'data' => $data,
          'method' => 'PUT',
          'returnHeader' => false,
          'headers' => ['X-Auth-Token: ' . session('token'), 'Content-type: application/json'],
          'endpoint' => ":8774/v2/servers/" . $id
        ];
        $this->_setopWithData($options);
        $exec = curl_exec($this->_ch);
        \Logger::write("(".__METHOD__.") Responded: ". json_encode($exec));
        curl_close($this->_ch);
        return response()->json(json_decode($exec));
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function startInstance($id)
    {
      try {
        \Logger::write("(".__METHOD__.") ".session('user')." requested a start of instance (".$id.")");
        $this->_ch = curl_init();
        $data = json_encode([
            "os-start" => NULL
        ]);
        $options = [
          'data' => $data,
          'method' => 'POST',
          'returnHeader' => false,
          'headers' => ['X-Auth-Token: ' . session('token'), 'Content-type: application/json'],
          'endpoint' => ":8774/v2/servers/".$id."/action"
        ];
        $this->_setopWithData($options);
        $exec = curl_exec($this->_ch);
        $code = curl_getinfo($this->_ch, CURLINFO_HTTP_CODE);
        \Logger::write("(".__METHOD__.") Responded: " . $code);
        curl_close($this->_ch);
        return response()->json($code);
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function restartInstance($id)
    {
      try {
        \Logger::write("(".__METHOD__.") ".session('user')." requested a restart of instance (".$id.")");
        $this->_ch = curl_init();
        $data = json_encode([
          "reboot" => [
              "type" => "HARD"
          ]
        ]);
        $options = [
          'data' => $data,
          'method' => 'POST',
          'returnHeader' => false,
          'headers' => ['X-Auth-Token: ' . session('token'), 'Content-type: application/json'],
          'endpoint' => ":8774/v2/servers/".$id."/action"
        ];
        $this->_setopWithData($options);
        $exec = curl_exec($this->_ch);
        $code = curl_getinfo($this->_ch, CURLINFO_HTTP_CODE);
        \Logger::write("(".__METHOD__.") Responded: ". $code);
        curl_close($this->_ch);
        return response()->json($code);
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function pauseInstance($id)
    {
      try {
        \Logger::write("(".__METHOD__.") ".session('user')." requested a pausing of instance (".$id.")");
        $this->_ch = curl_init();
        $data = json_encode([
            "pause" => NULL
        ]);
        $options = [
          'data' => $data,
          'method' => 'POST',
          'returnHeader' => false,
          'headers' => ['X-Auth-Token: ' . session('token'), 'Content-type: application/json'],
          'endpoint' => ":8774/v2/servers/".$id."/action"
        ];
        $this->_setopWithData($options);
        $exec = curl_exec($this->_ch);
        $code = curl_getinfo($this->_ch, CURLINFO_HTTP_CODE);
        \Logger::write("(".__METHOD__.") Responded: ". $code);
        curl_close($this->_ch);
        return response()->json($code);
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function shutdownInstance($id)
    {
      try {
        \Logger::write("(".__METHOD__.") ".session('user')." requested a shutdown of instance (".$id.")");
        $this->_ch = curl_init();
        $data = json_encode([
          "reboot" => [
              "type" => "HARD"
          ]
        ]);
        $options = [
          'data' => $data,
          'method' => 'POST',
          'returnHeader' => false,
          'headers' => ['X-Auth-Token: ' . session('token'), 'Content-type: application/json'],
          'endpoint' => ":8774/v2/servers/".$id."/action"
        ];
        $this->_setopWithData($options);
        $exec = curl_exec($this->_ch);
        $code = curl_getinfo($this->_ch, CURLINFO_HTTP_CODE);
        \Logger::write("(".__METHOD__.") Responded: ". $code);
        curl_close($this->_ch);
        return response()->json($code);
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }
}
