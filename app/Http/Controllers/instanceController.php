<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class instanceController extends Controller
{
    private $_ch;
    private $_auth;
    private $_flavor;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->_auth = session('token');
        $this->middleware('token');
    }

    private function _setopWithData($curlopt)
    {
      try {
        curl_setopt($this->_ch, CURLOPT_URL, env('API_ACCESS') . $curlopt['endpoint']);
        curl_setopt($this->_ch, CURLOPT_CUSTOMREQUEST, $curlopt['method']);
        curl_setopt($this->_ch, CURLOPT_POSTFIELDS, $curlopt['data']);
        curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->_ch, CURLOPT_HEADER, $curlopt['returnHeader']);
        curl_setopt($this->_ch, CURLOPT_HTTPHEADER, $curlopt['headers']);
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    private function _setop($curlopt)
    {
      try {
        curl_setopt($this->_ch, CURLOPT_URL, env('API_ACCESS') . $curlopt['endpoint']);
        curl_setopt($this->_ch, CURLOPT_CUSTOMREQUEST, $curlopt['method']);
        curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->_ch, CURLOPT_HEADER, $curlopt['returnHeader']);
        curl_setopt($this->_ch, CURLOPT_HTTPHEADER, $curlopt['headers']);
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function listFlavors()
    {
      $this->_ch = curl_init();
      $this->_ch = curl_init();
      $options = [
        'method' => 'GET',
        'returnHeader' => false,
        'headers' => ['X-Auth-Token: ' . session('token'), 'Content-type: application/json'],
        'endpoint' => ":8774/v2/flavors"
      ];
      $this->_setop($options);
      $exec = curl_exec($this->_ch);
      return json_decode($exec)->flavors;
    }

    private function _pow2($number)
    {
      return pow(2,ceil(log($number,2)));
    }

    private function _flavorize($data)
    {
      return [
        "C" . $data['cpu'],
        "R" . $this->_pow2($data['memory']),
        "D" . $data['storage'] * 1024
      ];
    }

    public function suggestFlavor(Request $request)
    {
      try {
        \Logger::write("Input: " . json_encode($request->data));
        $this->_flavor = $this->_flavorize($request->data);
        \Logger::write("(".__METHOD__.") Flavorized: " . json_encode($this->_flavor));
        $flavors = $this->listFlavors();
        \Logger::write("Flavors: " . json_encode($flavors));
        $available = array_map(function($row) {
          preg_match('/(C\d+)(R\d+)(D\d+)/', $row->name, $match);
          return [ 'id' => $row->id, 'name' => $match ];
        }, $flavors);
        \Logger::write("(".__METHOD__.") Available: " . json_encode($available));
        /**
         *  This is set to "strict" which can be loosen
         *  by setting the second if up with an OR operator
         *  and using the statement in the third as it's OR.
         */
        $matching = array_map(function($row) {
          \Logger::write("(".__METHOD__.") Matching: " . json_encode($row));
          if($this->_flavor[0] == $row['name'][1]) {
            if($this->_flavor[1] == $row['name'][2]) {
              if($this->_flavor[2] == $row['name'][3]) {
                return $row;
              }
            }
          }
        }, $available);
        \Logger::write("(".__METHOD__.") Responded: " . json_encode(array_values(array_filter($matching))));
        return response()->json(array_values(array_filter($matching)));
      } catch(Exception $e) {
        abort();
      }
    }

    public function createKeypair($key)
    {
      try {
        $this->_ch = curl_init();
        $data = [
          "keypair" => [
            "name" => "keypair-" . str_random(20),
            "public_key" => $key,
            "type" => 'ssh'
          ]
        ];
        $options = [
          'data' => json_encode($data),
          'method' => 'POST',
          'returnHeader' => false,
          'headers' => ['X-Auth-Token: ' . session('token'), 'Content-type: application/json'],
          'endpoint' => ":8774/v2/os-keypairs"
        ];
        $this->_setop($options);
        $exec = curl_exec($this->_ch);
        dd($exec);
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function generateKeypair($key)
    {
      try {
        $this->_ch = curl_init();
        $data = [
          "keypair" => [
            "name" => "keypair-" . str_random(20),
            "type" => 'ssh'
          ]
        ];
        $options = [
          'data' => json_encode($data),
          'method' => 'POST',
          'returnHeader' => false,
          'headers' => ['X-Auth-Token: ' . session('token'), 'Content-type: application/json'],
          'endpoint' => ":8774/v2/os-keypairs"
        ];
        $this->_setop($options);
        $exec = curl_exec($this->_ch);
        dd($exec);
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function create(Request $request)
    {
      try {
        \Logger::write("Create instance Input: " . json_encode($request->data) . " Keypair: " . json_encode($request->keypair));
        if(isset($request->keypair)) {
          $this->createKeypair($request->keypair);
        }
        $this->_ch = curl_init();
        $data = json_decode($request->data);
        $options = [
          'data' => json_encode($data),
          'method' => 'POST',
          'returnHeader' => false,
          'headers' => ['X-Auth-Token: ' . session('token'), 'Content-type: application/json'],
          'endpoint' => ":8774/v2/servers"
        ];
        $this->_setopWithData($options);
        $exec = curl_exec($this->_ch);
        \Logger::write(json_encode($exec));
        curl_close($this->_ch);
        return response()->json(json_decode($exec));
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function index()
    {
        return view('pages.createInstance');
    }
}
