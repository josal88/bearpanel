<?php
/**
 *  @author Johan Öhman Saldes
 *  @version 0.1
 *
 *  This controller is created to perform validation of input data from a view instance.
 *  Once the input has been validated it will preceed to push the appropiate variables
 *  to Openstack Neutron API and parse response on success. Elsewise it responds with
 *  it's own error responses.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class createNetworkController extends Controller
{
    private $_ch;

    /**
     *  @var _errorMsg array
     *  Contains errors to be sent back to client
     */

    private $_errorMsg;
    /**
     *  @var _successMsg array
     *  Contains success message to be sent back to client
     */
    private $_successMsg = [];

    /**
     *  @var _network_id array
     *  Contains network ID from successful network creation
     *  to be used for future association with subnet
     */
    private $_network_id;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('token');
    }

    private function _setopWithData($curlopt)
    {
      try {
        curl_setopt($this->_ch, CURLOPT_URL, env('API_ACCESS') . $curlopt['endpoint']);
        curl_setopt($this->_ch, CURLOPT_CUSTOMREQUEST, $curlopt['method']);
        curl_setopt($this->_ch, CURLOPT_POSTFIELDS, $curlopt['data']);
        curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->_ch, CURLOPT_HEADER, $curlopt['returnHeader']);
        curl_setopt($this->_ch, CURLOPT_HTTPHEADER, $curlopt['headers']);
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    /**
     *  Validate IP Addr
     *
     *  @param $ip valid ip address ex. 192.168.0.1
     *  @return bool
     */
    private function validateIpAddr($ip) {
        if(!filter_var($ip, FILTER_VALIDATE_IP) === false) {
            return true;
        }
        return false;
    }

    /**
     *  Validate CIDR with regexp
     *
     *  @param $ip valid ip address ex. 192.168.0.0/24
     *  @return bool || @param $cidr
     */
    private function validateCidr($cidr) {
        $re = "/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}(\/[0-9]{1,2})?$/";
        if (!preg_match($re, $cidr)) {
            return false;
        }
        return true;
    }
    /**
     *  Validate and normalising the allocation pools structure
     *  to be passed along to OS request.
     *
     *  @param string allocation pools
     *  @return array
     */
    private function validateAllocationPool($allocation_pools) {
        $ipArr = preg_split("/\,[\s]*/", $allocation_pools);
        $result = array_map(function($ip) {
            if(!filter_var($ip, FILTER_VALIDATE_IP) === false) {
                return $ip;
            }
        }, $ipArr);

        if(count($result) == 2) {
            return [ [ 'start' => $result[0], 'end' => $result[1] ] ];
        }
        return false;
    }

    private function validateName($name) {
        if( !empty($name) && is_string($name) ) {
            return true;
        }
        return false;
    }

    /**
     *  Decides how to structure the array to be passed on to OS.
     *
     *  @param array $subnet
     *  @param array $details
     *
     *  @return array
     */
    private function makeSubnetDataArr($subnet, $details) {
        return [
            "subnet" => [
                "network_id" => $this->_network_id,
                "name" => $subnet['name'],
                "ip_version" => 4,
                "cidr" => $subnet['netaddr'],
                "dns_nameservers" => [$details['dns']],
                "allocation_pools" =>  $this->validateAllocationPool($details['allocation_pool']),
                "gateway_ip" => $subnet['gateway']
            ]
        ];
    }

    private function createSubnet($subnet, $details) {
        $this->_ch = curl_init();

        if(!is_array($subnet)||!is_array($details)) {
            $this->_errorMsg = "Unable to parse data.";
            return false;
        }
        if( !$name = ( $name = $this->validateName($subnet['name']) ? true : false ) ) {
            $this->_errorMsg = "Invalid subnet name.";
            return false;
        }

        if( !$cidr = ( $cidr = $this->validateCidr($subnet['netaddr']) ? true : false ) ) {
            $this->_errorMsg = "Invalid CIDR IP range.";
            return false;
        }

        if( !$allocation_pools = ( $allocation_pools = $this->validateAllocationPool($details['allocation_pool']) ? true : false ) ) {
            $this->_errorMsg = "Invalid allocation pool range.";
            return false;
        }

        if( !$dns = ( $dns = $this->validateIpAddr($details['dns']) ? true : false ) ) {
            $this->_errorMsg = "Invalid DNS.";
            return false;
        }

        if( !$gateway = ( $gateway = $this->validateIpAddr($subnet['gateway']) ? true : false ) ) {
            $this->_errorMsg = "Invalid Gateway IP.";
            return false;
        }
        $options = [
            'data' => json_encode( $this->makeSubnetDataArr($subnet, $details) ),
            'method' => 'POST',
            'returnHeader' => false,
            'headers' => [
                'X-Auth-Token: ' . session('token'),
                'Content-type: application/json'
            ],
            'endpoint' => ":9696/v2.0/subnets"
        ];

        $this->_setopWithData($options);
        $exec = curl_exec($this->_ch);
        $respCode = curl_getinfo($this->_ch, CURLINFO_HTTP_CODE);
        curl_close($this->_ch);

        if(!$respCode == 200) {
            $this->_errorMsg = "Unable to create subnet. ( ERRNO: " . $respCode . " )";
            return false;
        }

        array_push($this->_successMsg, json_decode($exec));

        return true;
    }

    private function createNetwork($network) {
        if(!is_array($network)) {
            $this->_errorMsg = "Unable to parse data.";
            return false;
        }
        if( !$name = ( $name = $this->validateName($network['name']) ? true : false ) ) {
            $this->_errorMsg = "Invalid network name.";
            return false;
        }

        $data = [
            "network" => [
                "name" => $network['name'],
                "admin_state_up" => $network['state']
            ]
        ];

        $options = [
            'data' => json_encode($data),
            'method' => 'POST',
            'returnHeader' => false,
            'headers' => ['X-Auth-Token: ' . session('token'), 'Content-type: application/json'],
            'endpoint' => ":9696/v2.0/networks"
        ];

        $this->_setopWithData($options);
        $exec = curl_exec($this->_ch);
        $respCode = curl_getinfo($this->_ch, CURLINFO_HTTP_CODE);
        curl_close($this->_ch);

        if(!$respCode == 200) {
            $this->_errorMsg = "Unable to create network. ( ERRNO: " . $respCode . " )";
            return false;
        }
        // Check for quota exceed or other Neutron related errors
        if(isset(json_decode($exec)->NeutronError->message)) {
            $this->_errorMsg = json_decode($exec)->NeutronError->message;
            return false;
        }
        array_push($this->_successMsg, json_decode($exec));

        if( !is_object( $this->_successMsg[0] ) ) {
            $this->_errorMsg = "Unable to parse Network ID.";
            return false;
        }

        \Logger::write(json_encode($this->_successMsg[0]));
        $this->_network_id = $this->_successMsg[0]->network->id;

        return true;
    }


    public function index(Request $request)
    {
        $this->_ch = curl_init();
        if( !$createNetwork = ( $createNetwork = $this->createNetwork( $request->data['network'] ) ? true : false ) ) {
            \Logger::write($this->_errorMsg);
            return response()->json([ "error" => "true", "msg" => $this->_errorMsg ]);
        }
        if( !$createSubnet = ( $createSubnet = $this->createSubnet( $request->data['subnet'], $request->data['details'] ) ? true : false ) ) {
            \Logger::write($this->_errorMsg);
            return response()->json([ "error" => "true", "msg" => $this->_errorMsg ]);
        }
        \Logger::write(json_encode($this->_successMsg[0]));
        \Logger::write(json_encode($this->_successMsg[1]));
        return response()->json($this->_successMsg);
    }
}
