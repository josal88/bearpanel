<?php
/**
 *  This does keystone AUTHENTICATION and TOKEN handling. Nothing else.
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class KeystoneController extends Controller
{
    private $_ch;
    private $_tenantId;
    private $_token;
    private $_userId;

    /**
     *  Sets up our keystone verification HTTP request
     *  @param $auth
     *      json data containing username and password can be FALSE
     *  @param $endpoint
     *      API endpoint we want to access
     */
    private function _setop($curlopt)
    {
      try {
        curl_setopt($this->_ch, CURLOPT_URL, env('API_ACCESS') . $curlopt['endpoint']);
        curl_setopt($this->_ch, CURLOPT_CUSTOMREQUEST, $curlopt['method']);
        if($curlopt['auth'])
          curl_setopt($this->_ch, CURLOPT_POSTFIELDS, $curlopt['auth']);
        curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->_ch, CURLOPT_HEADER, $curlopt['returnHeader']);
        curl_setopt($this->_ch, CURLOPT_HTTPHEADER, $curlopt['headers']);
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    private function get_headers_from_curl_response($response)
    {
        $headers = array();
        $header_text = substr($response, 0, strpos($response, "\r\n\r\n"));
        foreach (explode("\r\n", $header_text) as $i => $line) {
            if ($i === 0)
              $headers['http_code'] = $line;
            else
            {
              list ($key, $value) = explode(': ', $line);
              $headers[$key] = $value;
            }
        }
        return $headers;
    }

    private function _checkTenantId()
    {
      try {
        $this->_ch = curl_init();
        $headers = array(
          'X-Auth-Token:' . session('token')
        );
        $this->_setop([
          'auth' => false,
          'method' => 'GET',
          'returnHeader' => false,
          'headers' => $headers,
          'endpoint' => ":5000/v2.0/tenants"
        ]);
        if(count(json_decode(curl_exec($this->_ch))->tenants) <= 1) {
          return json_decode(curl_exec($this->_ch))->tenants[0]->id;
          return true;
        }
        return false;
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function getTenentId()
    {
      try {
        $this->_ch = curl_init();
        $headers = array(
          'X-Auth-Token:' . session('token')
        );
        $this->_setop([
          'auth' => false,
          'method' => 'GET',
          'returnHeader' => false,
          'headers' => $headers,
          'endpoint' => ":5000/v2.0/tenants"
        ]);

        $exec = json_decode(curl_exec($this->_ch));
        curl_close($this->_ch);

        // We also make sure to update the token
        session(['token' => $this->getRealToken()]);

        if(count($exec->tenants) <= 1) {
          session(['tenant_id' => $exec->tenants[0]->id]);
        }
        return response()->json($exec);
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function getTenentIdById($id)
    {
      try {
        // curl -H "X-Auth-Token:$OS_AUTH_TOKEN" http://172.31.11.131:5000/v2.0/tenants
        $this->_ch = curl_init();
        $headers = array(
          'X-Auth-Token:' . session('token')
        );
        $this->_setop([
          'auth' => false,
          'method' => 'GET',
          'returnHeader' => false,
          'headers' => $headers,
          'endpoint' => ":5000/v2.0/tenants"
        ]);

        session(['tenant_id' => $id]);
        session(['token' => $this->getRealToken()]);

        return \Redirect::back();
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    private function getRealToken() {
      try {
        // curl -k -X 'POST' -v http://172.31.11.131:5000/v2.0/tokens -d '{"auth":{"passwordCredentials":{"username": "admin", "password":"secret123"}, "tenantId":"cd47b70525c64640a174e865573a7a81"}}' -H 'Content-type: application/json'
        $this->_ch = curl_init();
        $this->_auth = json_encode(["auth" => [
          "passwordCredentials" => [
            "username" => session('user'),
            "password" => session('pass')],
            "tenantId" => session('tenant_id')
          ]
        ]);
        $this->_setop([
          'auth' => $this->_auth,
          'method' => 'POST',
          'returnHeader' => false,
          'headers' => ['Content-type: application/json'],
          'endpoint' => ":5000/v2.0/tokens"
        ]);
        $exec = json_decode(curl_exec($this->_ch));
        if(isset($exec->error->code)) {
          return false;
        }
        return $exec->access->token->id;
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function index(Request $request) {
      try {
        $this->_ch = curl_init();
        // We need to implement a good salted random generated crypto here! :)
        session(['user' => $request->username, 'pass' => $request->password]);
        $this->_auth = json_encode(["auth" => [
                                      "passwordCredentials" => [
                                        "username" => session('user'),
                                        "password" => session('pass')
                                      ],
                                    "tenantId" => ""
                                    ]
                                  ]);
        $headers = array(
          'Content-Type: application/json',
          'Content-Length: ' . strlen($this->_auth)
        );

        $this->_setop([
          'auth' => $this->_auth,
          'method' => 'POST',
          'returnHeader' => false,
          'headers' => $headers,
          'endpoint' => ":5000/v2.0/tokens"
        ]);

        $response = json_decode(curl_exec($this->_ch));
        if(isset($response->error)) {
          \Logger::write("(".__METHOD__.") User ".session('user')." did an incorrect password attempt from IP ".$_SERVER['REMOTE_ADDR']." using ". $_SERVER['HTTP_USER_AGENT']);
          return \Redirect::back()->with('msg', 'Username and/or password was incorrect, please try again.');
        }

        /**
         *  I've only had problems here if there was no active internet connection and I presume there could
         *  be an error thrown here if the service in unavailable as in, keystone doesn't work properly.
         */
        if(!isset($response->access->token->id) || !isset($response->access->user->id)) {
          \Logger::write("(".__METHOD__.") User ".session('user')." did an incorrect password attempt from IP ".$_SERVER['REMOTE_ADDR']." using ". $_SERVER['HTTP_USER_AGENT']);
          return \Redirect::back()->with('status', 'Service is temporarily unavailable, please check your internet connection.');
        }

        session([
          'token' => $response->access->token->id,
          'user_id' => $response->access->user->id
        ]);
        // Now with these sessions set above, we'll see if we can get
        // one tenent or if the user has more tenant id's available
        session(['tenant_id' => $this->_checkTenantId()]);
        \Logger::write("(".__METHOD__.") User ".session('user')." was successfully logged in from ".$_SERVER['REMOTE_ADDR'].".");
        return \Redirect::to('/home');
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function check()
    {
      $this->_ch = curl_init();
      curl_setopt($this->_ch, CURLOPT_URL, env('API_ACCESS') . ":5000/v3/auth/tokens");
      curl_setopt($this->_ch, CURLOPT_CUSTOMREQUEST, "HEAD");
      curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->_ch, CURLOPT_HTTPHEADER, array(
          'X-Auth-Token: ' . session('token'),
          'X-Subject-Token: ' . session('token')
        )
      );


      $response = curl_exec($this->_ch);

      return curl_getinfo($this->_ch, CURLINFO_HTTP_CODE);
    }

    public function destroy()
    {
      $this->_ch = curl_init();
      curl_setopt($this->_ch, CURLOPT_URL, env('API_ACCESS') . ":5000/v3/auth/tokens");
      curl_setopt($this->_ch, CURLOPT_CUSTOMREQUEST, "DELETE");
      curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->_ch, CURLOPT_HEADER, 1);
      curl_setopt($this->_ch, CURLOPT_HTTPHEADER, array(
        'X-Auth-Token: ' . session('token'),
        'X-Subject-Token: ' . session('token')
      ));
      \Session::flush();
      curl_exec($this->_ch);
      curl_close($this->_ch);
      return \Redirect::to('/');
    }

    public function __destruct()
    {
      if(isset($this->_ch)) {
        curl_close($this->_ch);
      }
    }
}
