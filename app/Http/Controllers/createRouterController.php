<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class createRouterController extends Controller
{
  private $_ch;
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('token');
  }

  private function _setopWithData($curlopt)
  {
    try {
      curl_setopt($this->_ch, CURLOPT_URL, env('API_ACCESS') . $curlopt['endpoint']);
      curl_setopt($this->_ch, CURLOPT_CUSTOMREQUEST, $curlopt['method']);
      curl_setopt($this->_ch, CURLOPT_POSTFIELDS, $curlopt['data']);
      curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->_ch, CURLOPT_HEADER, $curlopt['returnHeader']);
      curl_setopt($this->_ch, CURLOPT_HTTPHEADER, $curlopt['headers']);
    } catch(Exception $e) {
      abort(403, 'Unauthorized.');
    }
  }

  private function makeRequestData($data) 
  {
    return json_encode([
      "router" => [
        "name" => $data['name'],
        "admin_state_up" => $data['state']
      ]
    ]);
  }

  public function index(Request $request)
  {
    $this->_ch = curl_init();
    if(empty($request->data['name'])) {
      return response()->json([ 'error' => '' ]);
    }
    $req = [
      'data' => $this->makeRequestData($request->data), 
      'method' => 'POST', 
      'returnHeader' => false, 
      'headers' => ['X-Auth-Token: ' . session('token'), 'Content-type: application/json'], 
      'endpoint' => ":9696/v2.0/routers" 
    ];

    $this->_setopWithData($req);
    $res = json_decode(curl_exec($this->_ch));
    return response()->json($res);
    curl_close($this->_ch);
  }
}
