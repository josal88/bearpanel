<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class blockStorageController extends Controller
{
    private $_ch;
    private $_auth;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->_auth = session('token');
      $this->middleware('token');
    }

    private function _setop($auth, $method, $endpoint, $headers, $head)
    {
      try {
        curl_setopt($this->_ch, CURLOPT_URL, env('API_ACCESS') . $endpoint);
        curl_setopt($this->_ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->_ch, CURLOPT_HEADER, $head);
        curl_setopt($this->_ch, CURLOPT_HTTPHEADER, $headers);
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    public function getVolume()
    {
      try {
        $this->_ch = curl_init();
        $headers = [
          'X-Auth-Token: ' . session('token'),
          'Accept: application/json'
        ];
        $this->_setop($this->_auth, 'GET', ':8776/v2/'. session('tenant_id') .'/volumes/detail', $headers, false);
        return response()->json(json_decode(curl_exec($this->_ch)));
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }
}
