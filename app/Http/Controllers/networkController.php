<?php
/**
 *  @author Johan Öhman
 *  @version 0.1b
 *
 *  General purpose controller for handling Neutron related API calls, has two main curl
 *  wrappers of which one accepts post fields or json etc. to be attatched to the request.
 *
 *  @todo Need to refactor and split up this codebase so that too many functions aren't
 *  relying too much on the logic of one single controller.
 *
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Keystone;

class networkController extends Controller
{
  private $_ch;
  private $_auth;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('token');
  }

  private function _setopWithData($curlopt)
  {
    try 
    {
      curl_setopt($this->_ch, CURLOPT_URL, env('API_ACCESS') . $curlopt['endpoint']);
      curl_setopt($this->_ch, CURLOPT_CUSTOMREQUEST, $curlopt['method']);
      curl_setopt($this->_ch, CURLOPT_POSTFIELDS, $curlopt['data']);
      curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->_ch, CURLOPT_HEADER, $curlopt['returnHeader']);
      curl_setopt($this->_ch, CURLOPT_HTTPHEADER, $curlopt['headers']);
    } 
    catch(Exception $e) 
    {
      abort(403, 'Unauthorized.');
    }
  }

  private function _setop($auth, $method, $endpoint, $headers = array(), $head)
  {
    try 
    {
      curl_setopt($this->_ch, CURLOPT_URL, env('API_ACCESS') . $endpoint);
      curl_setopt($this->_ch, CURLOPT_CUSTOMREQUEST, $method);
      curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->_ch, CURLOPT_HEADER, $head);
      curl_setopt($this->_ch, CURLOPT_HTTPHEADER, $headers);
    } catch(Exception $e) {
      abort(403, 'Unauthorized.');
    }
  }

  public function index()
  {
    return view('pages.networking');
  }

  public function listNetworks()
  {
    try 
    {
      $this->_ch = curl_init();
      $headers = [
        'X-Auth-Token: ' . session('token'),
        'Content-type: application/json'
      ];
      $this->_setop(session('token'), 'GET', ':9696/v2.0/networks.json?limit=2', $headers, false);
      return response()->json(json_decode(curl_exec($this->_ch)));
    } 
    catch(Exception $e) 
    {
      abort(403, 'Unauthorized.');
    }
  }

  public function getNetwork($id)
  {
    try 
    {
      $this->_ch = curl_init();
      $headers = [
        'X-Auth-Token: ' . session('token'),
        'Content-type: application/json'
      ];
      $this->_setop(session('token'), 'GET', ':9696/v2.0/networks/' . $id, $headers, false);
      return response()->json(json_decode(curl_exec($this->_ch)));
    } catch(Exception $e) {
      abort(403, 'Unauthorized.');
    }
  }

  public function deleteNetwork($id)
  {
    try 
    {
      $this->_ch = curl_init();
      $headers = [
        'X-Auth-Token: ' . session('token'),
        'Content-type: application/json'
      ];
      $this->_setop(session('token'), 'DELETE', ':9696/v2.0/networks/' . $id, $headers, false);
      curl_exec($this->_ch);
      curl_close($this->_ch);
      return \Redirect::back();
    } 
    catch(Exception $e) 
    {
      abort(403, 'Unauthorized.');
    }
  }

  public function listRouters()
  {
    try 
    {
      $this->_ch = curl_init();
      $headers = [
        'X-Auth-Token: ' . session('token'),
        'Content-type: application/json'
      ];
      $this->_setop(session('token'), 'GET', ':9696/v2.0/routers', $headers, false);
      return response()->json(json_decode(curl_exec($this->_ch)));
    } 
    catch(Exception $e) 
    {
      abort(403, 'Unauthorized.');
    }
  }

  public function getRouters($id)
  {
    try 
    {
      $this->_ch = curl_init();
      $headers = [
        'X-Auth-Token: ' . session('token'),
        'Content-type: application/json'
      ];
      $this->_setop(session('token'), 'GET', ':9696/v2.0/routers/' . $id, $headers, false);
      return response()->json(json_decode(curl_exec($this->_ch)));
    } 
    catch(Exception $e) 
    {
      abort(403, 'Unauthorized.');
    }
  }

  public function deleteRouter($id)
  {
    try
    {
      $this->_ch = curl_init();
      $headers = [
        'X-Auth-Token: ' . session('token'),
        'Content-type: application/json'
      ];
      $this->_setop(session('token'), 'DELETE', ':9696/v2.0/routers/' . $id, $headers, false);
      curl_exec($this->_ch);
      curl_close($this->_ch);
      return \Redirect::back();
    } 
    catch(Exception $e) 
    {
      abort(403, 'Unauthorized.');
    }
  }

  public function updateNetwork(Request $request, $id)
  {
    try 
    {
      $this->_ch = curl_init();
      $options = [
          'data' => json_encode([ "network" => $request->data]),
          'method' => 'PUT',
          'returnHeader' => false,
          'headers' => ['X-Auth-Token: ' . session('token'), 'Content-type: application/json'],
          'endpoint' => ":9696/v2.0/networks/". $id
        ];
        $this->_setopWithData($options);
        $exec = curl_exec($this->_ch);
        $respCode = curl_getinfo($this->_ch, CURLINFO_HTTP_CODE);
      return response()->json($respCode);
    } 
    catch(Exception $e) 
    {
      abort(403, 'Unauthorized.');
    }
  }

  public function routerDetails($id)
  {
    $router = function($id)
    {
      $this->_ch = curl_init();
      $headers = [
        'X-Auth-Token: ' . session('token'),
        'Content-type: application/json'
      ];
      $this->_setop(session('token'), 'GET', ':9696/v2.0/routers/' . $id, $headers, false);
      return json_decode(curl_exec($this->_ch));
    };

    return view('pages.routerDetails')->with('router', $router($id)->router)->with('id', $id);
  }

  public function createInterface(Request $request, $id)
  {
    try 
    {
        $this->_ch = curl_init();
        $data = json_encode([ "subnet_id" => $request->subnet_id ]);
        $options = [
          'data' => $data,
          'method' => 'PUT',
          'returnHeader' => false,
          'headers' => ['X-Auth-Token: ' . session('token'), 'Content-type: application/json'],
          'endpoint' => ":9696/v2.0/routers/". $id ."/add_router_interface"
        ];
        \Logger::write("(".__METHOD__.") Sent ". $options['method'] ." request to ".$options['endpoint']." with data: " . serialize($options['data']) ."");
        $this->_setopWithData($options);
        $exec = curl_exec($this->_ch);
        $respCode = curl_getinfo($this->_ch, CURLINFO_HTTP_CODE);
        \Logger::write($respCode." - ".json_encode($exec));
        curl_close($this->_ch);
        return response()->json($respCode);
      } 
      catch(Exception $e) 
      {
        abort(403, 'Unauthorized.');
      }
  }

  public function createGateway(Request $request, $id)
  {
    try 
    {
        $this->_ch = curl_init();

        $data = json_encode([
          "router" => [
            "external_gateway_info" => [
              "network_id" => $request->subnet_id,
              "enable_snat" => true,
            ]
          ]
        ]);
        $options = [
          'data' => $data,
          'method' => 'PUT',
          'returnHeader' => false,
          'headers' => ['X-Auth-Token: ' . Keystone::admin(), 'Content-type: application/json'],
          'endpoint' => ":9696/v2.0/routers/". $id
        ];
        \Logger::write("(".__METHOD__.") Sent ". $options['method'] ." request to ".$options['endpoint']." with data: " . serialize($options['data']) ."");
        $this->_setopWithData($options);
        $exec = curl_exec($this->_ch);
        $respCode = curl_getinfo($this->_ch, CURLINFO_HTTP_CODE);
        curl_close($this->_ch);
        \Logger::write($respCode." - ".json_encode($exec));
        if($respCode !== 200) {
          return response()->json(json_decode($exec));
        } else {
          return response()->json($respCode);
        }
      } 
      catch(Exception $e)
      {
        abort(403, 'Unauthorized.');
      }
  }

  public function listSubnets()
  {
    try 
    {
      $this->_ch = curl_init();
      $headers = [
        'X-Auth-Token: ' . session('token'),
        'Content-type: application/json'
      ];
      $this->_setop(session('token'), 'GET', ':9696/v2.0/subnets', $headers, false);
      return response()->json(json_decode(curl_exec($this->_ch)));
    } 
    catch(Exception $e)
    {
      abort(403, 'Unauthorized.');
    }
  }

  public function getSubnet($id)
  {
    try 
    {
      $this->_ch = curl_init();
      $headers = [
        'X-Auth-Token: ' . session('token'),
        'Content-type: application/json'
      ];
      $this->_setop(session('token'), 'GET', ':9696/v2.0/subnets/' . $id, $headers, false);
      return response()->json(json_decode(curl_exec($this->_ch)));
    } 
    catch(Exception $e)
    {
      abort(403, 'Unauthorized.');
    }
  }

  public function listSecuritygroups()
  {
    try 
    {
      $this->_ch = curl_init();
      $headers = [
        'X-Auth-Token: ' . session('token'),
        'Content-type: application/json'
      ];
      $this->_setop(session('token'), 'GET', ':9696/v2.0/security-groups', $headers, false);
      return response()->json(json_decode(curl_exec($this->_ch)));
    } 
    catch(Exception $e)
    {
      abort(403, 'Unauthorized');
    }
  }

  public function getSecurityrules($id)
  {
    try 
    {
      $this->_ch = curl_init();
      $headers = [
        'X-Auth-Token: ' . session('token'),
        'Content-type: application/json'
      ];
      $this->_setop(session('token'), 'GET', ':9696/v2.0/security-groups/' . $id, $headers, false);
      return response()->json(json_decode(curl_exec($this->_ch)));
    } 
    catch(Exception $e)
    {
      abort(403, 'Unauthorized');
    }
  }

  public function listSecurityrules()
  {
    try 
    {
      $this->_ch = curl_init();
      $headers = [
        'X-Auth-Token: ' . session('token'),
        'Content-type: application/json'
      ];
      $this->_setop(session('token'), 'GET', ':9696/v2.0/security-group-rules', $headers, false);
      return response()->json(json_decode(curl_exec($this->_ch)));
    } 
    catch(Exception $e)
    {
      abort(403, 'Unauthorized');
    }
  }

  public function addSecurityRule(Request $request, $id)
  {
    try 
    {
      \Logger::write("(".__METHOD__.") Input (".$id."): ". json_encode($request->data));
      $this->_ch = curl_init();
      $data = json_encode([
          "security_group_id" => $id,
          "security_group_rule" => $request->data
      ]);
      $options = [
        'data' => $data,
        'method' => 'POST',
        'returnHeader' => false,
        'headers' => ['X-Auth-Token: ' . session('token'), 'Content-type: application/json'],
        'endpoint' => ":9696/v2.0/security-group-rules"
      ];
      $this->_setopWithData($options);
      $exec = curl_exec($this->_ch);
      \Logger::write("(".__METHOD__.") Responded: ". json_encode($exec));
      curl_close($this->_ch);
      return response()->json(json_decode($exec));
    } 
    catch(Exception $e)
    {
      abort(403, 'Unauthorized.');
    }
  }
}
