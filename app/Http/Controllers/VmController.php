<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class VmController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('token');
    }
    /**
     * Create virtual machines
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.createVm');
    }

    /**
     * Manage existing virtual machines
     *
     * @return \Illuminate\Http\Response
     */
    public function manage()
    {
        return view('pages.manageVm');
    }
}
