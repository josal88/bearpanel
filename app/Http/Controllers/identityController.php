<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class identityController extends Controller
{
    private $_ch;
    private $_auth;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->_auth = session('token');
      $this->middleware('token');
    }
    private function _setop($auth, $method, $endpoint, $headers = array(), $head)
    {
      try {
        curl_setopt($this->_ch, CURLOPT_URL, env('API_ACCESS') . $endpoint);
        curl_setopt($this->_ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->_ch, CURLOPT_HEADER, $head);
        curl_setopt($this->_ch, CURLOPT_HTTPHEADER, $headers);
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }
    public function catalog()
    {
      try {
        $this->_ch = curl_init();
        $headers = [
          'X-Auth-Token: ' . session('token'),
          'X-Subject-Token: '. session('token')
        ];
        $this->_setop($this->_auth, 'GET', ':5000/v2.0/tenants', $headers, true);
        dd(curl_exec($this->_ch));
        return response()->json(json_decode(curl_exec($this->_ch)));
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }

    /**
     *  This is to access the tenent API endpoint.
     *
     *  @return JSON with project/tenent list including descriptions,
     *          name, enabled etc.
     */
    public function tenants()
    {
      try {
        $this->_ch = curl_init();
        $headers = [
          'X-Auth-Token: ' . session('token'),
          'X-Subject-Token: '. session('token')
        ];
        $this->_setop($this->_auth, 'GET', ':5000/v2.0/tenants', $headers, false);
        return response()->json(json_decode(curl_exec($this->_ch)));
      } catch(Exception $e) {
        abort(403, 'Unauthorized.');
      }
    }
}
