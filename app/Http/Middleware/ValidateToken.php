<?php

namespace App\Http\Middleware;

use Closure;

class ValidateToken
{
    private $_ch;

    private function setop($token) {
        curl_setopt($this->_ch, CURLOPT_URL, env('KEYSTONE') . "/v3/auth/tokens");
        curl_setopt($this->_ch, CURLOPT_CUSTOMREQUEST, "HEAD");
        curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->_ch, CURLOPT_HTTPHEADER, array(
            'X-Auth-Token: ' . $token,
            'X-Subject-Token: ' . $token
          )
        );
    }
    /**
     * Handle an incoming request and validates token against keystone.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->_ch = curl_init();
        if(session('token')) {
          $this->setop(
            session('token') // Send our token from session
          );
        }

        $response = curl_exec($this->_ch);

        /**
         *  The HEAD request to /v3/auth/tokens returns 200 if it's a valid
         *  token and the requested request can preceed. Else we'll simply
         *  redirect back to the login page.
         */
        if(curl_getinfo($this->_ch, CURLINFO_HTTP_CODE) == 200) {
          return $next($request);
        }
        \Logger::write("(".__METHOD__.") User ".session('user')." was logged out due to inactivity (IP: ". $_SERVER['REMOTE_ADDR'] ." CODE: ".curl_getinfo($this->_ch, CURLINFO_HTTP_CODE).")");
        \Session::flush();
        curl_close($this->_ch);
        return \Redirect::to('/')->with('status', 'Your session has expired and you\'ve been logged out.');
    }
}
