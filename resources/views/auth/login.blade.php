@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-body">
          @if(session('msg'))
            <div class="alert alert-danger" style="text-align: center;">{{session('msg')}}</div>
          @endif
          @if(session('status'))
            <div class="alert alert-info" style="text-align: center;">{{session('status')}}</div>
          @endif
          <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth') }}">
            {{ csrf_field() }}
            <div class="form-group">
              <div class="col-md-6 col-md-offset-3">
                <input id="email" type="username" class="form-control" name="username" value="{{ old('email') }}" placeholder="Username" required autofocus>
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-6 col-md-offset-3">
                <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-3 col-md-offset-3">
                <div class="checkbox">
                <label>
                  <input type="checkbox" name="remember"> Remember Me
                </label>
                </div>
              </div>
              <div class="col-md-5 col-md-offset-1">
                <button type="submit" class="btn btn-primary">
                  Login
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
