@extends('layouts.home')

@section('content')
<div id="home" class="container">
    <div class="row">
        <div class="col-md-2">
          @include('nav.sidebar')
        </div>
        <div class="col-md-8">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                  <div class="panel-heading">Last login</div>

                  <div class="panel-body">
                    Welcome back,<br>
                      Last login
                      <strong>
                        {{date('Y-m-d h:m:s')}}
                      </strong>
                      from
                      <strong>
                        {{$_SERVER['REMOTE_ADDR']}}
                      </strong>
                      connecting to
                      <strong>
                        {{$_SERVER['HTTP_HOST']}}
                      </strong>
                      using
                      <strong>
                        {{$_SERVER['HTTP_USER_AGENT']}}
                      </strong>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <div class="panel panel-default">
                      <div class="panel-heading">Nodes</div>

                      <div class="panel-body">
                          <canvas id="myChart" width="400" height="400"></canvas>
                      </div>
                  </div>
              </div>
          </div>
        </div>
        <div class="col-md-2">

        </div>
    </div>
</div>
@endsection
