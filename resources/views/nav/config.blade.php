{{--
    This is the menu to the right in the navigation bar
--}}
<ul id="navigation" class="nav navbar-nav navbar-right">
    <!-- Authentication Links -->
    <li v-if="auth" class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
          {{ session('user') }} <span class="caret"></span>
        </a>

        <ul class="dropdown-menu" role="menu">
            <li>
              <a href="{{ url('/home') }}">
                Dashboard
              </a>
            </li>
            <li>
              <a href="{{ url('/profile') }}">
                Profile
              </a>
            </li>
            <li>
              <a href="{{ url('/settings') }}">
                Settings
              </a>
            </li>
            <li>
                <a href="{{ url('/logout') }}">
                    Logout
                </a>

                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
    </li>
</ul>
