<div id="tenants" class="list-group">
  <div v-if="tenantsLoaded" class="list-group-item">
    <select class="form-control">
      <option v-for="tenant in tenants" v-bind:value="tenant.id" v-on:click="loadTenantId">
        @{{ tenant.name }}
      </option>
    </select>
  </div>
  <div v-else="tenantsLoaded" class="list-group-item">
    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
  </div>
</div>
@if(session('tenant_id'))
  <div class="list-group">
    <a href="{{url('/compute')}}" class="list-group-item">Compute</a>
    <a href="{{url('/networking')}}" class="list-group-item">Network</a>
    <a href="{{url('/orchestration')}}" class="list-group-item">Orchestration</a>
    <a href="{{url('/objectstore')}}" class="list-group-item">Object Store</a>
  </div>
  <div class="list-group">
    <a href="{{url('/create/instance')}}" class="list-group-item">Create Instance</a>
  </div>
  {{-- We don't need this as of now =)
  <div class="list-group">
  <a href="{{url('/projects')}}" class="list-group-item">Projects</a>
  </div>
  <div class="list-group">
  <a href="{{url('/create/virtualmachine')}}" class="list-group-item">Create VM</a>
  <a href="{{url('/manage/virtualmachine')}}" class="list-group-item">Manage VM</a>
  </div>
  <div class="list-group">
  <a href="{{url('/create/virtualmachine')}}" class="list-group-item">Licens Manager</a>
  </div>
  --}}
@endif
<script>
new Vue({
  el: '#tenants',
  data: {
    tenants: [],
    tenantsLoaded: false,
    loadedIfOne: false,
  },
  methods: {
    loadTenantId: function(e) {
      location = '/set/tenantid/' + e.target.value;
    }
  },
  ready: function() {
    var self = this;
    $.get('/tenants', function(data) {
      self.tenants = data['tenants']
      self.tenantsLoaded = true;
    });
  }
});
</script>
