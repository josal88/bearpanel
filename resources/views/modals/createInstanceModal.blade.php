<div
  class="modal fade bs-created-modal-lg"
  tabindex="-1" role="dialog"
  aria-labelledby="myLargeModalLabel"
  >
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div v-if="onCreatedLoaded" class="row">
            <h1>@{{ created.server.name }} was created <label class="btn btn-success">sucessfully</label></h1>
            <ul class="list-group">
              <li class="list-group-item">
                <div class="row">
                  <div class="col-md-4">
                    <strong>Admin Password</strong>
                  </div>
                  <div v-if="onCreated" class="col-md-8">
                    @{{onCreated.server.adminPass}}
                  </div>
                  <div v-else="onCreated" class="col-md-8">
                    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                  </div>
                </div>
              </li>
              <li class="list-group-item">
                <div class="row">
                  <div class="col-md-4">
                    <strong>id</strong>
                  </div>
                  <div v-if="onCreated" class="col-md-8">
                    @{{onCreated.server.id}}
                  </div>
                  <div v-else="onCreated" class="col-md-8">
                    <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                  </div>
                </div>
              </li>
            </ul>
          </div>
          <div v-else="onCreatedLoaded" style="text-align: center;" class="form-group">
            <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
