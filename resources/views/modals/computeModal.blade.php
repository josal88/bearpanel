<div class="modal fade bs-instance-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <table v-if="loadedInstance" class="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Status</th>
                <th>IP Address</th>
                <th>Size</th>
                <th>Key pair</th>
                <th>Availability zone</th>
                <th>Task</th>
                <th>Power state</th>
                <th>Time Since Created</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row"><a title="@{{ tables.instance.server.id }}">#</a></th>
                <td>@{{ tables.instance.server.status }}</td>
                <td>@{{ tables.instance.server.addresses.admin_internal_net[0].addr }}</td>
                <td>@{{ tables.flavor }}</td>
                <td v-if="!tables.instance.server.key_name">-</td>
                <td v-if="tables.instance.server.key_name">@{{ tables.instance.server.key_name }}</td>
                <td>@{{ tables.instance.server['OS-EXT-AZ:availability_zone'] }}</td>
                <td v-if="!tables.instance.server['OS-EXT-STS:task_state']">-</td>
                <td v-else="tables.instance.server['OS-EXT-STS:task_state']">@{{ tables.instance.server['OS-EXT-AZ:availability_zone'] }}</td>
                <td v-if="!tables.instance.server['OS-EXT-STS:power_state']"><button class="btn btn-danger btn-xs">Off</button></td>
                <td v-else="tables.instance.server['OS-EXT-STS:power_state']"><button class="btn btn-success btn-xs">On</button></td>
                <td>@{{ tables.instance.server.updated }}</td>
                <td>@{{ tables.instance.server.status }}</td>
              </tr>
            </tbody>
          </table>
          <div v-else="loadedInstance" style="text-align: center; padding-top: 35px;">
            <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
          </div>
          <div class="progress">
            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="@{{ tables.instance.server.progress }}" aria-valuemin="0" aria-valuemax="100" style="width: @{{ tables.instance.server.progress }}%"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade bs-createInstance-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Create Instance</h4>
      </div>
      <div class="row">
        <div class="col-md-10 col-md-offset-1">

        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade bs-volume-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Volume info</h4>
      </div>
      <div v-if="loadedVolumes" class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="row">
            <div class="col-md-6">
              <h4>What is Volume?</h4>
              <div class="list-group">
                <a href="#" class="list-group-item">
                  <p class="list-group-item-text">
                    A volume is a detachable block storage device, similar to a
                    USB hard drive. You can attach a volume to only one instance.
                    To create and manage volumes.
                  </p>
                  <a
                    href="http://docs.openstack.org/user-guide/common/cli-manage-volumes.html"
                    class="list-group-item-text"
                  >
                    Read more
                  </a>
                </a>
              </div>
            </div>
            <div class="col-md-6">
              <h4>Volume Image Metadata</h4>
              <div class="list-group">
                <a href="#" class="list-group-item active">
                  <h4 class="list-group-item-heading">
                    @{{tables.volume.volume_image_metadata.image_name}}
                  </h4>
                  <p v-if="tables.volume.volume_image_metadata.description" class="list-group-item-text">
                    @{{tables.volume.volume_image_metadata.description}}
                  </p>
                  <p v-else="tables.volume.volume_image_metadata.description" class="list-group-item-text">
                    N/A
                  </p>
                </a>
                <div class="list-group-item active" style="color: black;">
                  <p class="list-group-item-text">
                    <ul class="list-group">
                      <li class="list-group-item">
                        <div class="row">
                          <div class="col-md-4">
                            <strong>Min. Ram</strong>
                          </div>
                          <div class="col-md-8">
                            @{{tables.volume.volume_image_metadata.min_ram}} mb
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="row">
                          <div class="col-md-4">
                            <strong>Min. Disk</strong>
                          </div>
                          <div class="col-md-8">
                            @{{tables.volume.volume_image_metadata.min_disk}} gb
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="row">
                          <div class="col-md-4">
                            <strong>Format</strong>
                          </div>
                          <div class="col-md-8">
                            @{{tables.volume.volume_image_metadata.disk_format}}
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="row">
                          <div class="col-md-4">
                            <strong>Arch</strong>
                          </div>
                          <div class="col-md-8">
                            @{{tables.volume.volume_image_metadata.architecture}}
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <div class="row">
                          <div class="col-md-4">
                            <strong>Img. Name</strong>
                          </div>
                          <div class="col-md-8">
                            @{{tables.volume.volume_image_metadata.image_name}}
                          </div>
                        </div>
                      </li>
                    </ul>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div v-else="loadedVolumes" style="text-align: center;" class="row">
        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
      </div>
    </div>
  </div>
</div>
@include('modals.modal.security')