{{-- SECURITY RULES --}}
<div class="modal fade bs-managerules-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Rules</h4>
      </div>
      <div class="row" v-if="loadedSecGroupsRules">
        <div class="col-md-12">
          <div class="list-group-item active" style="color: black; border-radius: 0px; height: 100%;">
            <p class="list-group-item-text">
              <ul class="list-group">
                <li class="list-group-item" v-for="rule in listCurrentRules">
                  <div class="row">
                    <div class="col-md-1" name="@{{rule.security_group_id}}" v-on:click="removeRule">
                      <button>
                        <i class="fa fa-minus"></i>
                      </button>
                    </div>
                    <div class="col-md-4">
                      @{{rule.direction}}
                    </div>
                    <div class="col-md-4">
                      @{{rule.ethertype}} / @{{rule.protocol}}
                    </div>
                    <div class="col-md-3">
                      @{{rule.port_range_min}} - @{{rule.port_range_max}}
                    </div>
                  </div>
                </li>
              </ul>
            </p>
          </div>
        </div>
      </div>
      <div v-else="loadedSecGroupsRules" class="row" style="text-align: center; padding: 10px;">
        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
      </div>
    </div>
  </div>
  {{-- ADD --}}
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add Rule</h4>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group" style="padding: 5px 15px;">
            <label for="rule">
              Rule
            </label>
            <select 
              id="rule" 
              class="form-control" 
              v-model="addNewRule.protocol"
            >
              <option value="tcp">
                Custom TCP Rule
              </option>
              <option value="udp">
                Custom UDP Rule
              </option>
              <option value="icmp">
                Custom ICMP Rule
              </option>
              <option value="custom">
                Other Protocol
              </option>
              <option value="all_icmp">
                ALL ICMP
              </option>
              <option value="all_tcp">
                ALL TCP
              </option>
              <option value="all_udp">
                ALL UDP
              </option>
              <option value="dns">
                DNS
              </option>
              <option value="http">
                HTTP
              </option>
              <option value="https">
                HTTPS
              </option>
              <option value="imap">
                IMAP
              </option>
              <option value="imaps">
                IMAPS
              </option>
              <option value="ldap">
                LDAP
              </option>
              <option value="ms_sql">
                MS SQL
              </option>
              <option value="mysql">
                MYSQL
              </option>
              <option value="pop3">
                POP3
              </option>
              <option value="pop3s">
                POP3S
              </option>
              <option value="rdp">
                RDP
              </option>
              <option value="smtp">
                SMTP
              </option>
              <option value="smtps">
                SMTPS
              </option>
              <option value="ssh">
                SSH
              </option>
            </select>
          </div>
          <div class="form-group" style="padding: 5px 15px;">
            <label for="rule">
              Direction
            </label>
            <select 
              id="rule" 
              class="form-control" 
              v-model="addNewRule.direction"
            >
              <option value="ingress">
                Ingress
              </option>
              <option value="egress">
                Egress
              </option>
            </select>
          </div>
          <div class="input-group" style="padding: 2px 15px;">
            <label for="rule">
              Open Type:&nbsp; 
            </label>
            <button class="btn btn-default btn-xs" v-on:click="makePort" value="port">
              Port
            </button>
            <button class="btn btn-default btn-xs" v-on:click="makePortRange" value="range">
              Port Range
            </button>
          </div>
          {{-- First is if "Port" was choosen --}}
          <div v-else="addRuleUI.makePortRange" class="form-group" style="padding: 5px 15px;">
            <label for="rule">
              Port
            </label>
            <input 
              id="rule" 
              class="form-control" 
              v-model="addNewRule.port_range_min"
              v-on:change="addPortMax"
            >
          </div>
          {{-- Second is if "Port Range" was choosen --}}
          <div v-if="addRuleUI.makePortRange" class="row">
            <div class="col-md-6">
              <div class="form-group" style="padding: 5px 15px;">
                <label for="rule">
                  Port min
                </label>
                <input 
                  id="rule" 
                  class="form-control" 
                  v-model="addNewRule.port_range_min"
                >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group" style="padding: 5px 15px;">
                <label for="rule">
                  Port max
                </label>
                <input 
                  id="rule" 
                  class="form-control" 
                  v-model="addNewRule.port_max"
                >
              </div>
            </div>
          </div>
          <div class="form-group" style="padding: 5px 15px;">
            <label for="rule">
              Remote *
            </label>
            <select 
              id="rule" 
              class="form-control" 
              v-model="addNewRule.remote"
            >
              <option value="cidr">
                CIDR
              </option>
              <option value="sg">
                Security Group
              </option>
            </select>
          </div>
          <div class="form-group" style="padding: 5px 15px;">
            <label for="rule">
              CIDR
            </label>
            <input 
              id="rule"
              class="form-control"
              placeholder="0.0.0.0/0" 
              v-model="addNewRule.cidr"
            >
          </div>
          <div class="form-group" style="padding: 5px 15px;">
            <button 
              class="btn btn-success pull-right" 
              v-on:click="addSecurityRule"
            >
              Add
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
