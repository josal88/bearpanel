<div class="modal fade bs-network-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <table v-if="networkDetails.have.loaded" class="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Status</th>
                <th>IP Address</th>
                <th>Size</th>
                <th>Key pair</th>
                <th>Availability zone</th>
                <th>Task</th>
                <th>Power state</th>
                <th>Time Since Created</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row"><a title="@{{ tables.instance.server.id }}">#</a></th>
                <td>@{{ networkDetails.available.status }}</td>
                <td>@{{ tables.instance.server.addresses.admin_internal_net[0].addr }}</td>
                <td>@{{ tables.flavor }}</td>
                <td v-if="!tables.instance.server.key_name">-</td>
                <td v-if="tables.instance.server.key_name">@{{ tables.instance.server.key_name }}</td>
                <td>@{{ tables.instance.server['OS-EXT-AZ:availability_zone'] }}</td>
                <td v-if="!tables.instance.server['OS-EXT-STS:task_state']">-</td>
                <td v-else="tables.instance.server['OS-EXT-STS:task_state']">@{{ tables.instance.server['OS-EXT-AZ:availability_zone'] }}</td>
                <td v-if="!tables.instance.server['OS-EXT-STS:power_state']"><button class="btn btn-danger btn-xs">Off</button></td>
                <td v-else="tables.instance.server['OS-EXT-STS:power_state']"><button class="btn btn-success btn-xs">On</button></td>
                <td>@{{ tables.instance.server.updated }}</td>
                <td>@{{ tables.instance.server.status }}</td>
              </tr>
            </tbody>
          </table>
          <div v-else="networkDetails.have.loaded" style="text-align: center; padding-top: 35px;">
            <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade bs-router-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add interface</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <div class="form-group">
              <select class="form-control" v-model="req.router.network">
                <option
                  v-for="network in networks.available"
                  v-bind:value="network.id"
                >
                  @{{network.name}}
                </option>
              </select>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="IP Address (Optional)">
            </div>
            <div class="form-group">
              <button class="btn btn-success">Apply</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
