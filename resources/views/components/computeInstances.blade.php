<div v-show="panels.instances" class="panel panel-default">
  <div class="panel-heading">
    <i class="fa fa-server" aria-hidden="true"></i> Instances
    <i v-on:click="viewInstances" class="fa fa-times pull-right"></i>
  </div>
  <div class="panel-body">
    <table v-if="loadedInstances" class="table">
      <caption>
        Listing instances.
        @if(\App\RBA::authWrite(session('user_id') ,'create_instance'))
          <a
            href="/create/instance"
            class="btn btn-default pull-right"
          >
            <i class="fa fa-cloud-upload" aria-hidden="true"></i> Create Instance
          </a>
        @endif
      </caption>
      <thead>
        <tr>
          <th>#</th>
          <th>Status</th>
          <th>Name</th>
          <th></th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="instance in listInstances">
          <th scope="row">
            <a title="@{{instance.id}}">
              #
            </a>
          </th>
          <td>@{{instance.status}}</td>
          <td title="@{{instance.id}}" v-on:keydown.enter="updateInstanceName">
            <span v-on:click="editInstanceName">
              @{{instance.name}}
            </span>
          </td>
          <td>
          </td>
          <td>
            <div class="input-group-btn">
              <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>
              <ul class="dropdown-menu">
                <li
                  id="@{{instance.id}}"
                  v-on:click="getInstanceInfo"
                  data-toggle="modal"
                  data-target=".bs-instance-modal-lg"
                >
                  <span id="@{{instance.id}}" style="padding-left: 10px; width: 100%; height: 100%;">
                    <i class="fa fa-info-circle"></i> Info
                  </span>
                </li>
                <li>
                  <span id="@{{instance.id}}" style="padding-left: 10px; width: 100%; height: 100%;">
                    <i class="fa fa-desktop"></i> Console
                  </span>
                </li>
                <li role="separator" class="divider"></li>
                <li>
                  <span v-on:click="startInstance(instance.id)" style="padding-left: 10px; width: 100%; height: 100%;">
                    <i class="fa fa-play" style="color: #5cb85c;" aria-hidden="true"></i> Start
                  </span>
                </li>
                <li>
                  <span v-on:click="pauseInstance(instance.id)" style="padding-left: 10px; width: 100%; height: 100%;">
                    <i class="fa fa-pause" style="color: #5bc0de;" aria-hidden="true"></i> Pause
                  </span>
                </li>
                <li>
                  <span v-on:click="restartInstance(instance.id)" style="padding-left: 10px; width: 100%; height: 100%;">
                    <i class="fa fa-repeat" style="color: #337ab7;"></i> Restart
                  </span>
                </li>
                <li>
                  <span v-on:click="startInstance(instance.id)" style="padding-left: 10px; width: 100%; height: 100%;">
                    <i class="fa fa-stop" style="color: #d9534f;" aria-hidden="true"></i> Shutdown
                  </span>
                </li>
                <li role="separator" class="divider"></li>
                <li
                  id="@{{instance.id}}"
                  v-on:click="deleteInstance"
                >
                  <span id="@{{instance.id}}" style="padding-left: 10px; width: 100%; height: 100%;">
                    <i class="fa fa-trash"></i> Remove
                  </span>
                </li>
              </ul>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
    <div v-else="loadedInstances" style="text-align: center;">
      <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
    </div>
  </div>
</div>
