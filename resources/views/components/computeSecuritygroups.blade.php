<div v-show="panels.security" class="panel panel-default">
  <div class="panel-heading">
    <i class="fa fa-server" aria-hidden="true"></i> Access & Security
    <i v-on:click="viewSecurity" class="fa fa-times pull-right"></i>
  </div>
  <div class="panel-body">
    <div v-if="loadedSecGroups" class="form-group">
      <table class="table">
        @if(\App\RBA::authWrite(session('user_id') ,'create_instance'))
          <a
            href="/create/instance"
            class="btn btn-default pull-right"
          >
            <i class="fa fa-users" aria-hidden="true"></i> Create Security Group
          </a>
        @endif
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Description</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="group in listSecGroups">
            <th scope="row"><a title="@{{group.id}}">#</a></th>
            <td>@{{group.name}}</td>
            <td>@{{group.description}}</td>
            <td>
              <div class="input-group-btn">
                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>
                <ul class="dropdown-menu">
                  <li>
                    <a
                      v-on:click="getSecurityRules(group.id)"
                      data-toggle="modal"
                      data-target=".bs-managerules-modal-lg"
                    >
                      <i class="fa fa-pencil"></i> Manage Rules
                    </a>
                  </li>
                  <li role="separator" class="divider"></li>
                  <li>
                    <a name="@{{group.id}}" v-on:click="#">
                      <i class="fa fa-trash"></i> Remove
                    </a>
                  </li>
                </ul>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div v-else="loadedSecGroups" class="form-group" style="padding: 15px; text-align: center;">
      <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
    </div>
  </div>
</div>
