@extends('layouts.projects')

@section('content')
<div id="projects" class="container">
  <div class="row">
      <div class="col-md-2">
        @include('nav.sidebar')
      </div>
      <div class="col-md-8">
          <div class="panel panel-default">
            <div class="panel-heading">
              <i class="fa fa-server" aria-hidden="true"></i> Projects
            </div>
            <div class="panel-body">
              <table class="table">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Enabled</th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="project in projects">
                    <td>@{{project.name}}</td>
                    <td>@{{project.description}}</td>
                    <td v-if="project.enabled">Yes</td>
                    <td v-else="project.enabled">No</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
