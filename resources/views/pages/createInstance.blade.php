@extends('layouts.createInstance')

@section('content')
<div id="createInstance" class="container">
    <!-- Modal -->
    @include('modals.createInstanceModal')
    <div class="row">
        <div class="col-md-2">
          @include('nav.sidebar')
        </div>
        <div class="col-md-8" v-if="onCreatedLoaded">
          <div class="panel panel-default">
            <div class="panel-heading">
              
            </div>
            <div class="panel-body">
              <h3>Success!</h3>
              <p>You're instance has been successfully created and is now accessible using the following information</p>
              <table class="table">
                <thead>
                  <tr>
                    <th>Status</th>
                    <th>Name</th>
                    <th>Admin password</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>@{{newServerInfo.server.status}}</td>
                    <td>@{{newServerInfo.server.name}}</td>
                    <td>@{{onCreated.server.adminPass}}</td>
                  </tr>
                </tbody>
              </table>
              <p>
                <strong>ACTIVE</strong> means your instance is fully functional. <strong>BUILD</strong> means it's building and <strong>ERROR</strong> means something went wrong.
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-8" v-else="onCreatedLoaded">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                  <div class="panel-heading">Details</div>

                  <div class="panel-body">
                    <div class="form-group">
                      <label for="">Instance Name</label>
                      <input type="text" name="" class="form-control" v-model="created.server.name">
                    </div>
                    <div class="form-group">
                      <label for="">Availability Zone</label>
                      <select class="form-control" v-model="created.server.availability_zone">
                        <option v-for="availabilityzone in availabilityzone.available" v-bind:value="availabilityzone.zoneName">
                          @{{availabilityzone.zoneName}}
                        </option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="">Count</label>
                      <div class="input-group">
                        <div class="input-group-btn">
                          <button class="btn btn-default" v-on:click="decNode">-</button>
                        </div>
                        <input type="text" v-model="details.nodes" class="form-control" aria-label="...">
                        <div class="input-group-btn">
                          <button class="btn btn-default" v-on:click="incNode">+</button>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="panel panel-default">
                  <div class="panel-heading">Resources</div>

                  <div class="panel-body">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group" v-if="flavors.have.selected" style="text-align: center;">
                          <p>Resource type has been selected.<p>
                          <div class="list-group-item active" style="color: black;">
                            <p class="list-group-item-text">
                              <ul class="list-group">
                                <li class="list-group-item">
                                  <div class="row">
                                    <div class="col-md-4">
                                      <strong>Name</strong>
                                    </div>
                                    <div class="col-md-8">
                                      @{{flavors.choosen.flavor.name}}
                                    </div>
                                  </div>
                                </li>
                                <li class="list-group-item">
                                  <div class="row">
                                    <div class="col-md-4">
                                      <strong>CPU</strong>
                                    </div>
                                    <div class="col-md-8" v-text="flavors.choosen.flavor.vcpus | CPU">

                                    </div>
                                  </div>
                                </li>
                                <li class="list-group-item">
                                  <div class="row">
                                    <div class="col-md-4">
                                      <strong>RAM</strong>
                                    </div>
                                    <div class="col-md-8" v-text="flavors.choosen.flavor.ram | RAM">
                                      
                                    </div>
                                  </div>
                                </li>
                                <li class="list-group-item">
                                  <div class="row">
                                    <div class="col-md-4">
                                      <strong>Disk</strong>
                                    </div>
                                    <div class="col-md-8" v-text="flavors.choosen.flavor.disk | DISK">

                                    </div>
                                  </div>
                                </li>
                              </ul>
                            </p>
                          </div>
                        </div>
                        <div v-else="flavors.have.selected">
                          <div class="form-group">
                            <label id="storageInfo" for="storage">
                              Storage @{{system.storage}} GB
                            </label>
                            <input
                              id="storage"
                              type="range"
                              min="1"
                              max="1000"
                              value="1"
                              v-on:change="changeStorage"
                              v-model="system.storage"
                              >
                          </div>
                          <div class="form-group">
                            <label id="memoryInfo" for="memory">
                              Memory @{{system.memory}} MB
                            </label>
                            <input
                              id="memory"
                              type="range"
                              min="64"
                              max="32768"
                              step="64"
                              value="64"
                              v-on:change="changeMemory"
                              v-model="system.memory"
                              >
                          </div>
                          <div class="form-group">
                            <label id="cpuInfo" for="cpu">
                              CPU Cores @{{system.cpu}}
                            </label>
                            <input
                              id="cpu"
                              type="range"
                              min="1"
                              max="16"
                              value="1"
                              v-on:change="changeCpu"
                              v-model="system.cpu"
                              >
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row" v-if="flavors.exist">
                      <div class="col-md-12">
                        <table class="table">
                          <thead>
                            <tr>
                              <th>Flavor</th>
                              <th>VCPU core(s)</th>
                              <th>RAM (mb)</th>
                              <th>Disk (mb)</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr v-for="flavor in flavors.available">
                              <td>@{{flavor.name[0]}}</td>
                              <td v-text="flavor.name[1] | crdTransform"></td>
                              <td v-text="flavor.name[2] | crdTransform"></td>
                              <td v-text="flavor.name[3] | crdTransform"></td>
                              <td>
                                <button
                                  class="btn btn-default btn-xs"
                                  name="@{{flavor.id}}"
                                  v-on:click="chooseFlavor"
                                >
                                  Select
                                </button>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="row" v-if="!flavors.have.selected">
                      <div class="col-md-12">
                        <button
                          class="btn btn-success"
                          v-on:click="fetchResource"
                          v-if="!flavors.create"
                        >
                          Fetch Resources  
                        </button>
                        <button
                          class="btn btn-success"
                          v-on:click="createResource"
                          v-if="flavors.create"
                        >
                          Create Resource  
                        </button>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="panel panel-default">
                  <div class="panel-heading">Source</div>

                  <div class="panel-body">
                    <div v-if="sources.have.loaded" class="form-group">
                      <select class="form-control" v-model="created.server.imageRef">
                        <option
                          v-for="source in sources.available"
                          v-bind:value="source.id"
                          v-on:click="loadSource"
                        >
                          @{{ source.name }}
                        </option>
                      </select>
                    </div>
                    <div v-else="sources.have.loaded" style="text-align: center;" class="form-group">
                      <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                    </div>
                  </div>
              </div>
              <div class="panel panel-default">
                  <div class="panel-heading">Networks</div>

                  <div class="panel-body">
                    <div v-if="networks.have.loaded" class="form-group">
                      <select class="form-control">
                        <option
                          v-for="network in networks.available"
                          v-bind:value="network.id"
                          v-on:click="loadFlavor"
                        >
                          @{{ network.name }}
                        </option>
                      </select>
                    </div>
                    <div v-else="networks.have.loaded" style="text-align: center;" class="form-group">
                      <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                    </div>
                  </div>
              </div>
              <div class="panel panel-default">
                  <div class="panel-heading">Security Groups</div>

                  <div class="panel-body">
                    <div v-if="secruitygroups.have.loaded" class="form-group">
                      <select class="form-control">
                        <option
                          v-for="secruitygroup in secruitygroups.available"
                          v-bind:value="secruitygroup.id"
                          v-on:click="loadFlavor"
                        >
                          @{{ secruitygroup.description }}
                        </option>
                      </select>
                    </div>
                    <div v-else="secruitygroups.have.loaded" style="text-align: center;" class="form-group">
                      <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                    </div>
                  </div>
              </div>
              <div class="panel panel-default">
                  <div class="panel-heading">Key Pair</div>

                  <div class="panel-body">
                    <div class="row">
                      <div class="col-md-6" v-if="createKeypair">
                        <div class="form-group">
                          <button class="btn btn-default" v-on:click="uploadFile">
                              <i class="fa fa-upload" aria-hidden="true"></i> Import Key Pair
                          </button>
                        </div>
                      </div>
                      <div class="col-md-6" v-if="createKeypair">
                        <div class="form-group">
                          <button class="btn btn-default">
                            <i class="fa fa-plus" aria-hidden="true"></i> Auto generate Key Pair
                          </button>
                        </div>
                      </div>
                      <div class="col-md-12" v-else="createKeypair">
                        <div class="form-group">
                          <input
                            type="text"
                            class="form-control"
                            placeholder="ssh-rsa AAAAB3NzaC1yc2EAAAA..."
                            v-model="keypair"
                          >
                        </div>
                      </div>
                    </div>
                    <div class="row" v-if="createKeypair">
                      <div class="col-md-12" v-if="keypairs.have.loaded">
                        <table class="table">
                          <thead>
                            <tr>
                              <th>
                                Name
                              </th>
                              <th>
                                Fingerprint
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr v-for="key in keypairs.available">
                              <td>@{{key.keypair.name}}</td>
                              <td>@{{key.keypair.fingerprint}}</td>
                              <td>
                                <button
                                  class="btn btn-default btn-xs"
                                  name="@{{key.keypair.name}}"
                                  v-on:click="selectKeypair"
                                >
                                  Select
                                </button>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div v-else="keypairs.have.loaded" style="text-align: center;" class="form-group">
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                    </div>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">Configuration</div>

                    <div class="panel-body">
                      <div class="form-group">
                        <label for="customscript">Customization Script</label>
                        <textarea
                        id="customscript"
                        class="form-control"
                        v-model="created.server.user_data"
                        >
                      </textarea>
                    </div>
                    <div class="form-group">
                      <button class="btn btn-default" v-on:click="base64EncodeUserData">Encode</button>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6"></div>
                  <div class="col-md-6">
                    {{-- --}}
                    <div class="form-group">
                      <button
                        type="submit"
                        class="btn btn-success pull-right"
                        v-on:click="submitServer"
                        class="btn btn-info btn-xs"
                        data-toggle="modal"
                        data-target=".bs-created-modal-lg"
                      >
                        Create Server
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="col-md-2">

        </div>
    </div>
</div>
@endsection
