@extends('layouts.routerDetails')

@section('content')
<div id="router" class="container">
  {{-- @include('modals.networking')
{#239 ▼
  +"status": "ACTIVE"
  +"external_gateway_info": {#240 ▼
    +"network_id": "78a995cb-3a0b-48b9-8275-b65db1de8194"
    +"enable_snat": true
    +"external_fixed_ips": array:1 [▼
      0 => {#241 ▼
        +"subnet_id": "e0545a5c-8176-433a-948c-f9ab0cc2c49d"
        +"ip_address": "172.31.11.152"
      }
    ]
  }
  +"availability_zone_hints": []
  +"availability_zones": array:1 [▼
    0 => "nova"
  ]
  +"description": ""
  +"admin_state_up": true
  +"tenant_id": "386de7f7703d4e1fb007f0ea0d590737"
  +"routes": []
  +"id": "b8ac175d-f153-496e-864c-ef0813713c44"
  +"name": "Example Router"
}
  --}}
  <div class="row">
    <div class="col-md-2">
      @include('nav.sidebar')
    </div>
    <div class="col-md-8">
      <div class="row">
        <p class="list-group-item-text" style="color: black;">
          <ul class="list-group">
            <li class="list-group-item">
              <div class="row">
                <div class="col-md-4">
                  <strong>Status</strong>
                </div>
                <div class="col-md-8">
                  {{$router->status}}
                </div>
              </div>
            </li>
            <li class="list-group-item">
              <div class="row">
                <div class="col-md-4">
                  <strong>Description</strong>
                </div>
                <div class="col-md-8">
                  {{$router->description ? $router->description : "N/A"}}
                </div>
              </div>
            </li>
            @if(isset($router->external_gateway_info))
              <li class="list-group-item">
                <div class="row">
                  <div class="col-md-4">
                    <strong>Ext. Fixed IP</strong>
                  </div>
                  <div class="col-md-8">
                  </div>
                </div>
              </li>
              @foreach($router->external_gateway_info->external_fixed_ips as $ip)
                <li class="list-group-item">
                  <div class="row">
                    <div class="col-md-4">
                      <strong><i class="fa fa-circle"></i></strong>
                    </div>
                    <div class="col-md-8">
                      {{$ip->ip_address}}
                    </div>
                  </div>
                </li>
              @endforeach
            @endif
            @if(isset($router->availability_zones))
              @foreach($router->availability_zones as $zone)
                <li class="list-group-item">
                  <div class="row">
                    <div class="col-md-4">
                      <strong>Availability Zone</strong>
                    </div>
                    <div class="col-md-8">
                      {{$zone}}
                    </div>
                  </div>
                </li>
              @endforeach
            @endif
            <li class="list-group-item">
              <div class="row">
                <div class="col-md-4">
                  <strong>Admin state</strong>
                </div>
                <div class="col-md-8">
                  {{$router->admin_state_up ? 'UP' : 'DOWN'}}
                </div>
              </div>
            </li>
          </ul>
        </p>
      </div>
      <div class="panel panel-default">
        <div class="panel panel-heading">
          Interface
        </div>
        <div class="panel-body">
          <div class="form-group">
            <select class="form-control" v-model="choosen.interface.subnet_id">
              <option
                v-for="subnet in subnets.items"
                v-bind:value="subnet.id"
              >
                @{{subnet.name}}
              </option>
            </select>
          </div>
          <button class="btn btn-primary" name="{{$id}}" v-on:click="addInterface">
            Attatch Interface
          </button>

          <button class="btn btn-danger" name="{{$id}}" v-on:click="addInterface">
            Detatch Interface
          </button>
        </div>  
      </div>
      <div class="panel panel-default">
        <div class="panel panel-heading">
          Gateway
        </div>
        <div class="panel-body">
          <div class="form-group">
            <select class="form-control" v-model="choosen.gateway.subnet_id">
              <option
                v-for="subnet in networks.items"
                v-bind:value="subnet.id"
              >
                @{{subnet.name}}
              </option>
            </select>
          </div>
          <button class="btn btn-primary" name="{{$id}}" v-on:click="addGateway">
            Attatch Gateway
          </button>

          <button class="btn btn-danger" name="{{$id}}" v-on:click="addGateway">
            Detatch Gateway
          </button>
        </div>  
      </div>
    </div>
  </div>
</div>
@endsection
