@extends('layouts.networking')

@section('content')
<div id="networking" class="container">
    @include('modals.networking')
    <div class="row">
        <div class="col-md-2">
          @include('nav.sidebar')
        </div>
        <div class="col-md-8">
          <div class="panel panel-default">
            <div class="panel-heading">
              Networks
            </div>

              <div class="panel-body">
                <table v-if="networks.have.loaded" class="table">
                  <thead>
                    <tr>
                      <th>Status</th>
                      <th>Name</th>
                      <th>Shared</th>
                      <th>External</th>
                      <th>Admin</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr v-for="network in networkList">
                      <td>@{{network.status}}</td>
                      <td>@{{network.name}}</td>
                      <td v-if="network.shared">
                        <span class="label label-success">Shared</span>
                      </td>
                      <td v-else="network.shared">
                        <span class="label label-danger">Unshared</span>
                      </td>
                      <td>@{{network.router['external']}}</td>
                      <td v-if="network.admin_state_up">
                        <span class="label label-success">Up</span>
                      </td>
                      <td v-else="network.admin_state_up">
                        <span class="label label-danger">Down</span>
                      </td>
                      <td>
                        <div class="input-group-btn">
                          <button 
                            type="button" 
                            class="btn btn-default btn-xs dropdown-toggle" 
                            data-toggle="dropdown" 
                            aria-haspopup="true" 
                            aria-expanded="false">
                              Action <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu">
                            <li>
                              <a
                                v-on:click="editNetwork(network.id)"
                                style="
                                  padding-left: 10px; 
                                  width: 100%; 
                                  height: 100%; 
                                  cursor: pointer;
                                      "
                              >
                                <i class="fa fa-pencil" aria-hidden="true"></i> Edit
                              </a>
                            </li>
                            <li>
                              <a
                                v-on:click="moreNetworkInfo(network.id)"
                                data-toggle="modal"
                                data-target=".bs-network-modal-lg" 
                                style="
                                  padding-left: 10px; 
                                  width: 100%; 
                                  height: 100%; 
                                  cursor: pointer;
                                      "
                              >
                                <i class="fa fa-info-circle" aria-hidden="true"></i> More Info
                              </a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li
                              id="@{{instance.id}}"
                              v-on:click="deleteNetwork"
                            >
                              <a 
                                name="@{{network.id}}"
                                href="/networking/delete/@{{network.id}}" 
                                style="
                                  padding-left: 10px; 
                                  width: 100%; 
                                  height: 100%; 
                                  cursor: pointer;
                                      "
                              >
                                <i class="fa fa-trash"></i> Remove
                              </a>
                            </li>
                          </ul>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              <div v-else="networks.have.loaded" style="text-align: center; padding-top: 35px;">
                <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
              </div>
            </div>
          </div>
          {{-- Display network response message --}}
          <div v-show="alertSuccess" class="alert alert-success">
            <button type="button" class="close">
              <span aria-hidden="true" v-on:click="dismissSucessMsg">&times;</span>
            </button>
            @{{messages.successMsg}}
          </div>
          <div v-show="alertError" class="alert alert-danger">
            <button type="button" class="close">
              <span aria-hidden="true" v-on:click="dismissErrorMsg">&times;</span>
            </button>
            @{{messages.errorMsg}}
          </div>
          {{-- Edit or add network --}}
          <div class="panel panel-default">
            <div v-if="showEditNetwork" class="panel-heading">
              Edit Network
              <i v-on:click="displayNetworkPanel" class="fa fa-chevron-up pull-right"></i>
            </div>
            <div v-else="showEditNetwork" class="panel-heading">
              Add Network
              <i v-on:click="displayNetworkPanel" class="fa fa-chevron-up pull-right"></i>
            </div>
            <div v-if="showEditNetwork" class="panel-body" v-if="showNetworkPanel">
              <div v-if="currentNetworkEditReady">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input id="name" class="form-control" v-model="currentNetworkEdit.name">
                </div>
                <div class="form-group">
                  <input id="router" type="checkbox" v-model="currentNetworkEdit.router">
                  <label for="router">Router External</label>
                </div>
                <div class="form-group">
                  <input id="admin_state_up" type="checkbox" v-model="currentNetworkEdit.admin_state_up">
                  <label for="admin_state_up">Admin State Up</label>
                </div>
                <div class="form-group">
                  <input id="port_security_enabled" type="checkbox" v-model="currentNetworkEdit.port_security_enabled">
                  <label for="port_security_enabled">Port Security Enabled</label>
                </div>
                <div class="form-group">
                  <button 
                    class="btn btn-success" 
                    v-on:click="updateNetwork(currentNetworkEdit.id)"
                  >
                    Update Network
                  </button>
                </div>
              </div>
              <div v-else="currentNetworkEditReady" style="width: 100%; text-align: center; padding-top: 35px;">
                <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
              </div>
            </div>
            <div v-else="showEditNetwork" class="panel-body" v-if="showNetworkPanel">
              <div class="row">
                <div class="col-md-6">
                  <h4>Network</h4>
                  <div class="form-group">
                    <label>Network name</label>
                    <input type="text" class="form-control" v-model="req.vlan.network.name">
                  </div>
                  <div class="form-group">
                    <label>Admin state</label>
                    <select class="form-control" v-model="req.vlan.network.state">
                      <option value="true" selected="true">UP</option>
                      <option value="false">DOWN</option>
                    </select>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <input
                          id="id_shared"
                          name="shared"
                          disabled=""
                          type="checkbox"
                          v-model="req.vlan.network.share"
                        >
                        <label>Share</label>
                      </div>
                      <div v-if="showEditNetwork" class="form-group">
                        <input
                          checked="checked"
                          name="with_subnet"
                          type="checkbox"
                          v-model="req.vlan.network.with_subnet"
                        >
                        <label>Update Subnet</label>
                      </div>
                      <div v-else="showEditNetwork" class="form-group">
                        <input
                          checked="checked"
                          name="with_subnet"
                          type="checkbox"
                          v-model="req.vlan.network.with_subnet"
                        >
                        <label>Create Subnet</label>
                      </div>
                      <div class="form-group">
                        <input
                          checked="checked"
                          name="vlan_transparent"
                          type="checkbox"
                          v-model="req.vlan.network.vlan_transparent"
                        >
                        <label>VLAN Transparent</label>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <button
                        class="btn btn-success"
                        v-if="!req.vlan.network.with_subnet"
                        v-on:click="processNewNetwork"
                      >
                        Create Network
                      </button>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div v-if="req.vlan.network.with_subnet">
                    <h4>Subnet</h4>
                    <div class="form-group">
                      <label>Subnet Name</label>
                      <input type="text" class="form-control" v-model="req.vlan.subnet.name">
                    </div>
                    <div class="form-group">
                      <label>Network Address</label>
                      <input type="text" class="form-control" v-model="req.vlan.subnet.netaddr">
                    </div>
                    <div class="form-group">
                      <label>IP Version</label>
                      <select class="form-control" v-model="req.vlan.subnet.ipv">
                        <option value="4" selected="true">IPv4</option>
                        <option value="6">IPv6</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>Gateway IP</label>
                      <input type="text" class="form-control" v-model="req.vlan.subnet.gateway">
                    </div>
                  </div>
                </div>
              </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="well">
                      The OpenStack Networking service provides an API that
                      allows users to set up and define network connectivity
                      and addressing in the cloud. The project code-name for
                      Networking services is neutron. OpenStack Networking
                      handles the creation and management of a virtual
                      networking infrastructure, including networks, switches,
                      subnets, and routers for devices managed by the OpenStack
                      Compute service (nova). Advanced services such as
                      firewalls or virtual private networks (VPNs) can also be
                      used.
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div v-if="req.vlan.network.with_subnet">
                      <h4>Subnet details</h4>
                      <div class="form-group">
                        <label>Allocation Pools</label>
                        <input type="text" class="form-control" v-model="req.vlan.details.allocation_pool">
                      </div>
                      <div class="form-group">
                        <label>DNS Name Servers</label>
                        <input type="text" class="form-control" v-model="req.vlan.details.dns">
                      </div>
                      <div class="form-group">
                        <label>Host Routes</label>
                        <input type="text" class="form-control" v-model="req.vlan.details.host">
                      </div>
                      <div class="form-group">
                        <input
                          checked="checked"
                          name="vlan_transparent"
                          type="checkbox"
                          v-model="req.vlan.details.enable_dhcp"
                        >
                        <label>Enable DHCP</label>
                      </div>
                      <div class="form-group">
                        <button
                          class="btn btn-success"
                          v-if="req.vlan.network.with_subnet"
                          v-on:click="processNewNetwork"
                        >
                          Create Network
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                Routers
              </div>

                <div class="panel-body">
                  <table v-if="routers.have.loaded" class="table">
                    <thead>
                      <tr>
                        <th>Status</th>
                        <th>Name</th>
                        <th>Subnet</th>
                        <th>Subnet IP</th>
                        <th>Admin</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr v-for="router in routerList">
                        <td>@{{router.status}}</td>
                        <td><a href="/networking/router/@{{router.id}}">@{{router.name}}</a></td>
                        <td name="@{{router.external_gateway_info.external_fixed_ips[0].subnet_id}}"></td>
                        <td>@{{router.external_gateway_info.external_fixed_ips[0].ip_address}}</td>
                        <td>@{{router.admin_state_up}}</td>
                        <td>
                          <a
                            name="@{{router.id}}"
                            class="btn btn-danger btn-xs"
                            href="/router/delete/@{{router.id}}"
                          >
                            Remove
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                <div v-else="routers.have.loaded" style="text-align: center; padding-top: 35px;">
                  <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                </div>
              </div>
          </div>
            <div class="panel panel-default">
            <div class="panel-heading">
              Add Router
              <i v-on:click="displayRouterPanel" class="fa fa-chevron-up pull-right"></i>
            </div>

              <div class="panel-body" v-if="showRouterPanel">
                <div class="form-group">
                  <input class="form-control" type="text" v-model="req.router.name" placeholder="Router name">
                </div>
                <div class="form-group">
                  <select class="form-control" v-model="req.router.state">
                    <option value="true" selected="true">UP</option>
                    <option value="false">DOWN</option>
                  </select>
                </div>
                <div class="form-group">
                  <select class="form-control" v-model="req.router.network">
                    <option
                      v-for="network in networkList"
                      v-bind:value="network.id"
                      placeholder="Choose network"
                    >
                      @{{network.name}}
                    </option>
                  </select>
                </div>
                <div class="form-group">
                  <button v-on:click="processNewRouter" class="btn btn-success pull-right">Create</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-2">

        </div>
    </div>
</div>
@endsection
