@extends('layouts.compute')

@section('content')

<div id="compute" class="container">
    <!-- Modal -->
    @include('modals.computeModal')
    <div class="row">
        <div class="col-md-2">
          @include('nav.sidebar')
        </div>
        <div class="col-md-10">
          <ul class="nav nav-pills">
            <li v-on:click="viewOverview" role="presentation" id="overview" class="active">
              <a href="#">Overview</a>
            </li>
            <li v-on:click="viewInstances" role="presentation" id="instances">
              <a href="#">Instances</a>
            </li>
            <li v-on:click="viewVolumes" role="presentation" id="volumes">
              <a href="#">Volumes</a>
            </li>
            <li v-on:click="viewImages" role="presentation" id="images">
              <a href="#">Images</a>
            </li>
            <li v-on:click="viewSecurity" role="presentation" id="security">
              <a href="#">Access & Security</a>
            </li>
          </ul>
          <div v-show="panels.overview" class="panel panel-default">
            <div class="panel-heading">
              <i class="fa fa-server" aria-hidden="true"></i> Overview
              <i v-on:click="viewOverview" class="fa fa-times pull-right"></i>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-4">
                  <h3>Instances</h3>
                  <canvas id="instanceChart" width="200" height="200"></canvas>
                </div>
                <div class="col-md-4">
                  <h3>VCPUs</h3>
                  <canvas id="vcpusChart" width="400" height="400"></canvas>
                </div>
                <div class="col-md-4">
                  <h3>RAM</h3>
                  <canvas id="ramChart" width="400" height="400"></canvas>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <h3>Floating IPs</h3>
                  <canvas id="floatingIpChart" width="200" height="200"></canvas>
                </div>
                <div class="col-md-4">
                  <h3>Security Groups</h3>
                  <canvas id="securityGroupsChart" width="400" height="400"></canvas>
                </div>
                <div class="col-md-4">
                  <h3>Volumes</h3>
                  <canvas id="volumesChart" width="400" height="400"></canvas>
                </div>
              </div>
            </div>
          </div>
          {{--
            INSTANCES!
          --}}
          @include('components.computeInstances')
          {{--
            VOLUMES
          --}}
          <div v-show="panels.volumes" class="panel panel-default">
            <div class="panel-heading">
              <i class="fa fa-server" aria-hidden="true"></i> Volumes
              <i v-on:click="viewVolumes" class="fa fa-times pull-right"></i>
            </div>
            <div class="panel-body">
              <table v-if="loadedVolumes" class="table">
                <caption>Optional table caption.</caption>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="volume in listVolumes">
                    <th scope="row"><a title="@{{volume.id}}">#</a></th>
                    <td>@{{volume.name}}</td>
                    <td>@{{volume.description}}</td>
                    <td>@{{volume.status}}</td>
                    <td>
                      <button
                        name="@{{volume.id}}"
                        class="btn btn-info"
                        v-on:click="getVolumeInfo"
                        class="btn btn-info btn-xs"
                        data-toggle="modal"
                        data-target=".bs-volume-modal-lg"
                      >
                        More info
                      </button>
                    </td>
                  </tr>
                </tbody>
              </table>
              <div v-else="loadedInstances" style="text-align: center;">
                <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
              </div>
            </div>
          </div>
          <div v-show="panels.images" class="panel panel-default">
            <div class="panel-heading">
              <i class="fa fa-server" aria-hidden="true"></i> Images
              <i v-on:click="viewImages" class="fa fa-times pull-right"></i>
            </div>
            <div class="panel-body">
              <table v-if="loadedImages" class="table">
                <caption>Listing available images.</caption>
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Status</th>
                    <th>V.Size</th>
                    <th>Name</th>
                    <th>C.Format</th>
                    <th>Min.Size</th>
                    <th>Min.Ram</th>
                    <th>D.Format</th>
                    <th>Visibility</th>
                    <th>Protected</th>
                    <th>Created</th>
                    <th>Updated</th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="image in listImages">
                    <th scope="row"><a title="@{{image.id}}"></a></th>
                    <td>@{{image.status}}</td>
                    <td>@{{image.virtual_size}}</td>
                    <td>@{{image.name}}</td>
                    <td>@{{image.container_format}}</td>
                    <td>@{{image.size}}</td>
                    <td>@{{image.min_ram}}</td>
                    <td>@{{image.disk_format}}</td>
                    <td>@{{image.visibility}}</td>
                    <td>@{{image.min_disk}}</td>
                    <td v-text="image.created_at | momento"></td>
                    <td v-text="image.updated_at | momento"></td>
                  </tr>
                </tbody>
              </table>
              <div v-else="loadedImages" style="text-align: center;">
                <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
              </div>
            </div>
          </div>
          @include('components.computeSecuritygroups')
        </div>
    </div>
</div>
@endsection
