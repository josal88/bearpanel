@extends('layouts.createVm')

@section('content')
<div id="createVm" class="container">
    <div class="row">
        <div class="col-md-2">
          @include('nav.sidebar')
        </div>
        <div class="col-md-8">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                  <div class="panel-heading"><i class="fa fa-server" aria-hidden="true"></i> Operating System</div>

                  <div v-if="operatingSystem" class="panel-body">
                    <div class="col-md-6">
                      <div class="list-group">
                        <a href="#" class="list-group-item" style="height: 130px; padding: 25px 25px; overflow: hidden;">
                          <img id="debian" v-on:click="chooseOs" class="img-responsive" src="/img/os/debian-logo-horizontal.gif" alt="">
                        </a>
                        <a href="#" class="list-group-item" style="height: 130px; padding: 15px 25px; overflow: hidden;">
                          <img id="fedora" v-on:click="chooseOs" class="img-responsive" src="/img/os/fedora-logo.png" alt="">
                        </a>
                        <a href="#" class="list-group-item" style="height: 130px; padding-top: 0px; overflow: hidden;">
                          <img id="freebsd" v-on:click="chooseOs" class="img-responsive" src="/img/os/freebsd-logo.png" alt="">
                        </a>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="list-group">
                        <a href="#" class="list-group-item" style="height: 130px; padding: 20px 25px; overflow: hidden;">
                          <img id="redhat" v-on:click="chooseOs" class="img-responsive" src="/img/os/red-hat-logo.png" alt="">
                        </a>
                        <a href="#" class="list-group-item" style="height: 130px; padding: 25px 25px; overflow: hidden;">
                          <img id="ubuntu" v-on:click="chooseOs" class="img-responsive" src="/img/os/Ubuntu_logo.png" alt="">
                        </a>
                        <a href="#" class="list-group-item" style="height: 130px; padding: 35px 25px; overflow: hidden;">
                          <img id="windows" v-on:click="chooseOs" class="img-responsive" src="/img/os/windows-10-logo.png" alt="">
                        </a>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <i class="fa fa-server" aria-hidden="true"></i> Instant Application
                  <i v-on:click="displayInstaApp" class="fa fa-chevron-up pull-right"></i>
                </div>
                <transition name="instaApp">
                  <div v-if="instaApp" class="panel-body instaApp">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-10">
                            <img class="appIcon" src="/img/tech/wordpress.png"> Wordpress
                          </div>
                          <div class="col-md-2">
                            <input type="checkbox">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-10">
                            <img class="appIcon" src="/img/tech/joomla.png"> Joomla
                          </div>
                          <div class="col-md-2">
                            <input type="checkbox">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-10">
                            <img class="appIcon" src="/img/tech/drupal.jpeg"> Drupal
                          </div>
                          <div class="col-md-2">
                            <input type="checkbox">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-10">
                            <img class="appIcon" src="/img/tech/wikimedia.png"> Wikimedia
                          </div>
                          <div class="col-md-2">
                            <input type="checkbox">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-10">
                            <img class="appIcon" src="/img/tech/gitlab.png"> Gitlab
                          </div>
                          <div class="col-md-2">
                            <input type="checkbox">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-10">
                            <img class="appIcon" src="/img/tech/laravel.png"> Laravel
                          </div>
                          <div class="col-md-2">
                            <input type="checkbox">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-10">
                            <img class="appIcon" src="/img/tech/react.png"> React
                          </div>
                          <div class="col-md-2">
                            <input type="checkbox">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-10">
                            <img class="appIcon" src="/img/tech/angular.png"> AngularJS
                          </div>
                          <div class="col-md-2">
                            <input type="checkbox">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-10">
                            <img class="appIcon" src="/img/tech/django.png"> Django
                          </div>
                          <div class="col-md-2">
                            <input type="checkbox">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-10">
                            <img class="appIcon" src="/img/tech/ruby.png"> Ruby on Rails
                          </div>
                          <div class="col-md-2">
                            <input type="checkbox">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-10">
                            <img class="appIcon" src="/img/tech/mariadb.png"> MariaDB
                          </div>
                          <div class="col-md-2">
                            <input type="checkbox">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-10">
                            <img class="appIcon" src="/img/tech/redis.png"> Redis
                          </div>
                          <div class="col-md-2">
                            <input type="checkbox">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-10">
                            <img class="appIcon" src="/img/tech/nodejs.png"> NodeJS
                          </div>
                          <div class="col-md-2">
                            <input type="checkbox">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-10">
                            <img class="appIcon" src="/img/tech/docker.png"> Docker
                          </div>
                          <div class="col-md-2">
                            <input type="checkbox">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-10">
                            <img class="appIcon" src="/img/tech/couchdb.png"> CouchDB
                          </div>
                          <div class="col-md-2">
                            <input type="checkbox">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-10">
                            <img class="appIcon" src="/img/tech/nginx.png"> LEMP Stack
                          </div>
                          <div class="col-md-2">
                            <input type="checkbox">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-10">
                            <img class="appIcon" src="/img/tech/lamp.jpg"> LAMP Stack
                          </div>
                          <div class="col-md-2">
                            <input type="checkbox">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-10">
                            <img class="appIcon" src="/img/tech/mean.png"> Mean Stack
                          </div>
                          <div class="col-md-2">
                            <input type="checkbox">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-10">
                            <img class="appIcon" src="/img/tech/mysql.png"> MySQL
                          </div>
                          <div class="col-md-2">
                            <input type="checkbox">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-10">
                            <img class="appIcon" src="/img/tech/postgresql.png"> PostgreSQL
                          </div>
                          <div class="col-md-2">
                            <input type="checkbox">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                          <div class="col-md-10">
                            <img class="appIcon" src="/img/tech/mongodb.png"> MongoDB
                          </div>
                          <div class="col-md-2">
                            <input type="checkbox">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </transition>
              </div>
            </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <div class="panel panel-default">
                      <div class="panel-heading"><i class="fa fa-server" aria-hidden="true"></i> System specifications</div>

                      <div class="panel-body">
                        <div class="form-group">
                          <label id="storageInfo" for="storage">
                            Storage @{{system.storage}} GB
                          </label>
                          <input
                            id="storage"
                            type="range"
                            min="50"
                            max="1000"
                            value="50"
                            v-on:change="changeStorage"
                            v-model="system.storage"
                            >
                        </div>
                        <div class="form-group">
                          <label id="memoryInfo" for="memory">
                            Memory @{{system.memory}} MB
                          </label>
                          <input
                            id="memory"
                            type="range"
                            min="32"
                            max="32768"
                            value="32"
                            v-on:change="changeMemory"
                            v-model="system.memory"
                            >
                        </div>
                        <div class="form-group">
                          <label id="cpuInfo" for="cpu">
                            CPU Cores @{{system.cpu}}
                          </label>
                          <input
                            id="cpu"
                            type="range"
                            min="1"
                            max="16"
                            value="1"
                            v-on:change="changeCpu"
                            v-model="system.cpu"
                            >
                        </div>
                        <div class="form-group">
                          <label id="networkInfo" for="network">
                            Network @{{system.network}} Gbit / Month
                          </label>
                          <input
                            id="network"
                            type="range"
                            min="10"
                            max="1000"
                            value="10"
                            v-on:change="changeNetwork"
                            v-model="system.network"
                            >
                        </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="row">
            <div class="col-md-3">
              <div class="input-group">
                <div class="input-group-btn">
                  <button class="btn btn-default" v-on:click="removeNode">-</button>
                </div>
                <input type="text" v-model="amountOfNodes" class="form-control" aria-label="...">
                <div class="input-group-btn">
                  <button class="btn btn-default" v-on:click="addNode">+</button>
                </div>
              </div>
            </div>
            <div class="col-md-5">

            </div>
            <div class="col-md-4">
              <div class="input-group pull-right">
                <button class="btn btn-success">Launch</button>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-2">

        </div>
    </div>
</div>
@endsection
