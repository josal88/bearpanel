<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function() {
  return view('auth.login');
});
Route::get('/test', function() {
  return Logger::test();
});
/**
 *  Authentication
 */
Route::get('/login', function() {
  return view('auth.login');
});
/**
 *  Identity
 */
Route::get('/tenants', 'KeystoneController@getTenentId');
Route::get('/set/tenantid/{id}', 'KeystoneController@getTenentIdById');
Route::get('/catalog', 'identityController@catalog');
Route::post('/auth', 'KeystoneController@index');
Route::get('/logout', 'KeystoneController@destroy');
Route::get('/authenticated', 'KeystoneController@check');

Route::get('/home', 'HomeController@index');
Route::get('/profile', 'HomeController@profile');
Route::get('/settings', 'HomeController@settings');

Route::get('/create/instance', 'instanceController@index');

/**
 *  Projects
 */
Route::get('/projects', function() {
  return view('pages.projects');
});
/**
 *  Compute
 */
Route::get('/compute', 'computeController@index');
Route::get('/api/servers', 'computeController@listInstances');
Route::get('/api/server/{serverId}', 'computeController@getInstance');
Route::get('/api/images', 'computeController@listImages');
Route::get('/api/image/{imageId}', 'computeController@getImage');
Route::get('/api/flavors', 'computeController@getFlavors');
Route::get('/api/flavor/{flavorId}', 'computeController@getFlavor');
Route::get('/api/flavors/detail', 'computeController@getFlavorsWithDetails');
Route::get('/api/availabilityzone', 'computeController@getAvailabilityZone');
Route::get('/api/secruitygroups', 'computeController@getSecurityGroups');
Route::get('/api/keypairs', 'computeController@getKeypairs');
Route::get('/api/keypairs/new', 'computeController@generateKeypair');

Route::post('/create/server', 'instanceController@create');
Route::get('/delete/server/{server_id}', 'computeController@deleteServer');
Route::post('/update/server/{id}', 'computeController@updateServerName');

Route::post('/create/flavor', 'computeController@createFlavor');

Route::post('/suggest/flavor', 'instanceController@suggestFlavor');

/**
 *  Block Storage
 */
Route::get('/api/volumes', 'blockStorageController@getVolume');

/**
 *  Networking
 */
Route::get('/networking', 'networkController@index');
Route::get('/networking/router/{id}', 'networkController@routerDetails');

# Networks

Route::get('/api/networks', 'networkController@listNetworks');
Route::get('/api/network/{network_id}', 'networkController@getNetwork');
Route::post('/create/network', 'createNetworkController@index');
Route::get('/networking/delete/{network_id}', 'networkController@deleteNetwork');
Route::post('/networking/update/{network_id}', 'networkController@updateNetwork');

# Routers

Route::get('/api/routers', 'networkController@listRouters');
Route::get('/api/router/{router_id}', 'networkController@getRouters');
Route::post('/create/router', 'createRouterController@index');
Route::get('/router/delete/{router_id}', 'networkController@deleteRouter');
Route::post('/create/interface/{router_id}', 'networkController@createInterface');
Route::post('/create/gateway/{router_id}', 'networkController@createGateway');

# Subnets

Route::get('/api/subnets', 'networkController@listSubnets');
Route::get('/api/subnet/{subnet_id}', 'networkController@getSubnet');

# Security groups

Route::get('/api/security/groups', 'networkController@listSecuritygroups');

# Security groups
Route::get('/api/security/rules/{id}', 'networkController@getSecurityrules');
Route::post('/api/security/rules/{id}', 'networkController@addSecurityRule');
Route::get('/api/security/rules', 'networkController@listSecurityrules');


/**
 *  Create and manage virtual machine
 */
 Route::get('/create/virtualmachine', 'VmController@create');
 Route::get('/manage/virtualmachine', 'VmController@manage');

 Route::get('/debug/token', 'debugController@token');

/**
 *	Instance management
 */
Route::get('/api/server/start/{id}', 'computeController@startInstance');
Route::get('/api/server/restart/{id}', 'computeController@restartInstance');
Route::get('/api/server/pause/{id}', 'computeController@pauseInstance');
Route::get('/api/server/shutdown/{id}', 'computeController@shutdownInstance');

/**
 *  Debugging
 */
Route::get('/debug', function() {
  return "true";
});
